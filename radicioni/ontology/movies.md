|              Name             |    Director    |         Editor        |       Writer       |      Actor      |       Oscar       | Rating | Duration | Year |
|===============================|================|=======================|====================|=================|===================|========|==========|======|
| Blade Runner                  | Ridley Scott   | Terry Rawlings        | Philip K. Dick     | Harrison Ford   |                   |    4.1 |      117 | 1982 |
|                               |                | Marsha Nakashima      |                    | Rutger Hauer    |                   |        |          |      |
|                               |                |                       |                    | Sean Young      |                   |        |          |      |
|                               |                |                       |                    | Daryl Hannah    |                   |        |          |      |
|                               |                |                       |                    | Brion James     |                   |        |          |      |
|                               |                |                       |                    | Joanna Cassidy  |                   |        |          |      |
|-------------------------------|----------------|-----------------------|--------------------|-----------------|-------------------|--------|----------|------|
| Gladiator                     | Ridley Scott   | Pietro Scalia         | David Franzoni     | Russel Crowe    | Best film         |    4.2 |      155 | 2000 |
|                               |                |                       |                    | Joaquin Phoenix | (Ridley Scott)    |        |          |      |
|                               |                |                       |                    | Connie Nielsen  | Best Leader Actor |        |          |      |
|                               |                |                       |                    | Richard Harris  | (Russel Crowe)    |        |          |      |
|-------------------------------|----------------|-----------------------|--------------------|-----------------|-------------------|--------|----------|------|
| Gran Torino                   | Clint Eastwood | Joel Cox              | Nick Schenk        | Clint Eastwood  |                   |    4.1 |      116 | 2008 |
|                               |                |                       | Dave Johannson     | Bee Vang        |                   |        |          |      |
|                               |                |                       |                    | Ahney Her       |                   |        |          |      |
|-------------------------------|----------------|-----------------------|--------------------|-----------------|-------------------|--------|----------|------|
| Le ali della libertà          | Frank Darabont | Richard Francis Bruce | Stephen King       | Tim Robbins     |                   |    4.6 |      142 | 1994 |
| (the shawshank redemption)    |                |                       |                    | Morgan Freeman  |                   |        |          |      |
|                               |                |                       |                    | Bob Gunton      |                   |        |          |      |
|-------------------------------|----------------|-----------------------|--------------------|-----------------|-------------------|--------|----------|------|
| The Good,the Bad and the Ugly | Sergio Leone   | Eugenio Alabiso       | Sergio Leone       | Clint Eastwood  |                   |    4.5 |      161 | 1966 |
|                               |                | Nino Baragli          | Luciano Vincenzoni | Eli Wallach     |                   |        |          |      |
|                               |                |                       |                    | Lee Van Cleef   |                   |        |          |      |
|-------------------------------|----------------|-----------------------|--------------------|-----------------|-------------------|--------|----------|------|
|                               |                |                       |                    |                 |                   |        |          |      |







