# Sviluppo di un'ontologia

Ripercorrere le fasi principali del tutorial di Horridge (ProtegeOWLTutorial.pdf)

* Creazione di un'ontologia di dominio per un settore dei beni culturali: musica,cinema o letteratura.
    - L'ontologia deve utilizzare assiomi e restrizioni e deve essere popolata con un numero minimo di individui, che devono essere classificati 'correttamente'