# Slides di radizioni

## Lezione 0 - introduzione ontologie

### Concettualizzazione

Struttura formale di (una parte di) realtà percepita e organizzata da un agente, indipendentemente da: 

* **vocabolario** utilizzato
* **l'occorrenza** effettiva di una specifica situazione

Differenti situazioni che coinvolgono alcuni oggetti descritti da vocabolari differenti, possono condividere la stessa concettualizzazione (esempio: mela e apple)

### Ontologia e semantica

Sono strettamente collegate tra di loro: l'**ontologia** riguarda *ciò che è*, la semantica si occupa di *riferirsi a ciò che è*

### Knowledge base

* Componente **terminologica**: (ontologia)
    - independent of particular states of affairs
    - designed to support terminological services
* Componente **asserzionale**:
    - reflects specific (epistemic) states of affairs
    - designed for problem-solving

### Lessico

è un elenco di parole in una lingua. Per esempio un vocabolario con qualche forma di conoscenza su come si usano le parole

Relazioni lessicali: sinonimia, iponimia e iperonimia ecc...

La relazione di iponimia è molto simile ad una relazione di tipo *subclass-of*... idem per meronimia e *has-part*

La differenza è che nelle ontologie le **sottocategorie** sono mutualmente esclusive, mentre nel lessico esistono sovrapposizioni di significati

### Esempio di ontologia

    John:
        height: 6 feet      # intrinsic quality
        has-part: hands     # part
        has-employee: Jane  # role
        kissed: Mary        # external relation (event)
        job: researcher     # relational quality

### Entità vs eventi

**entità**: *oggetti* che continuano per un periodo di tempo mantenendo la propria identità (cellula, nucleo..)

**eventi**: *oggetti* che accadono, si svolgono o sviluppano nel tempo (replicazione di DNA, mitosi, divisione della cellula)

Termini utilizzati in queseta macro-distinzione: **endurant** e **perdurant**

### Upper ontologies (ontologie fondazionali)

Catturano un insieme di distinzioni base valide in vari domini

Esempio: **DOLCE**

### Relazione sublass-of

* A è subclass-of B
    - Ogni istanza di A è anche istanza di B (si tratta di una relazione di sottoinsieme)
        + Ogni essere umano è anche un mammifero
    - I valori dei componenti di B sono ereditati dalle istanze di A
        + Ogni essere umano è un animale vertebrato che respira aria

### Identity criteria

per valutare se due identità possono essere in relazione di sottoclasse, possiamo utilizzare la nozione di **criteri di identità**: le condizioni che utilizziamo per determinare l'eguaglianza, derivanti dall'identità

* I criteri di identità sono proprietà necessarie delle entità confrontate
    - **time-duration**: criterio di identità -> stessa lunghezza
    - **time-interval**: criterio di identità -> stesso inizio e stessa fine
    - Le due entità non possono essere in relazione di sottoclasse 

## Lezione 1 - rappresentazioni strutturate

### Sistema di rappresentazione della conoscenza

deve consistere di:

* un **linguaggio di rappresentazione**: un insieme di strutture sintattiche adatte a codificare le informazioni da rappresentare, implementabili in programmi.
* un **insieme di regole**, o di **operazioni**: per manipolare tali strutture sintattiche in accordo con il loro significato.

L'applicazione delle regole deve portare alle inferenze desiderate, e le regole devono poter essere formulate come procedure effettive

Per rappresentare la conoscenza si possono anche usare le **formule logiche**, ma sono limitate perchè non è possibile collegarle tra di loro, organizzandole in blocchi omogenei

### Reti semantiche

Classe di formalismi avente la caratteristica comune di adottare una **struttura a grafo** in cui

* **nodi** -> *concetti*
* **archi** -> *relazioni tra concetti* o *proprietà dei concetti*

#### Grafi relazionali

* nodi -> entità
* archi etichettati -> relazione tra i nodi (la più importante è isA)

##### Espressività

* i grafi relazionali implementano un sottoinsieme del **calcolo dei predicati del primo ordine**:
    - archi -> predicati
    - nodi -> termini
* limitazioni in fatto di efficacia espressiva:
    - tutte le relazioni sono messe in **congiunzione** tra loro
    - non posso rappresentare **disgiunzione** o **implicazione** (il blocco b è verde OR rosso)
    - difficile rappresentare l'idea di **quantificazione universale** (tutti i ragazzi amano qualche ragazza)
    - le relazioni sono unicamente **binarie**. non posso esprimere l'*arità*. (per esempio una regola ternaria come "Mario regala un libro a Maria -> regala(mario,libro,maria)")

Una soluzione al problema dell'arità è tradurre tutte le relazioni in relazioni binarie:

Mario regala un libro a Maria diventa un grafo:

* reg32
    - --- agente ---> mario
    - --- isa ---> regala
    - --- oggetto ---> libro13
        + --- isa ---> libro
    - --- ricevente ---> maria

#### Reti proposizionali

Reti in cui i nodi possono rappresentare non solo entità semplici ma intere proposizioni (Franco pensa che Maria creda che il suo (di lei) cane stia mangiando un osso)

Ora si può usare:

* connettivi logici e contesti all'interno dei quali fare operare i quantificatori
* negazione
* disgiunzione: come NOT(NOT(A) AND NOT(B))
    - Franco ama Maria o Franco ama Barbara -> Non è vero che Franco non ama Maria e che Franco non ama Barbara
* quantificatori (con qualche complicazione)

## Lezione 2 - KL-ONE

KL-ONE propone una vista bipartita dell'attività di rappresentazione della conoscenza

### Struttura del linguaggio

#### Terminologia vs statements

* si basa sulla distinzione tra i costrutti KL-ONE che hanno lo scopo di elaborare *descrizioni*, e quelli il cui scopo è di creare *affermazioni* (statement)

#### Tentativo epistempologico

* elemento principale di KL-ONE: *structured conceptual object*, o **Concept**
* linguaggio con un set di **elementi primitivi** per rappresentare un ampio spettro di concetti
* KL-ONE è un tentativo di determinare un insieme ragionevole per sottolineare **oggetti** e **tipi di relazioni** per strutturare la conoscenza
* non è una teoria di un particolare dominio, è ad un livello superiore, come una parte di una **teoria generativa** della **struttura** e dei **limiti** del *pensiero* di un agente razionale

KL-ONE tenta di capire le caratteristiche importanti della struttura interna dei concetti, e di incorporarle in un linguaggio che è fortemente espressivo

#### Concetti primitivi e definiti

KL-ONE separa le sue descrizioni in due gruppi principali: **primitive** e **defined**

Quando si specifica una conoscenza di dominio in KL-ONE, si parte specificando dei tipi primitivi, seguiti da altri tipi che sono definiti in termini di quelli primitivi.

##### Concetti primitivi

* es: PUNTO e SEGMENTO
* sono permessi anche concetti primitivi che hanno proprietà definite: essi vengono trattati come definizioni incomplete
    - POLIGONO è un esempio. ha molte proprietà, noi non vogliamo rappresentare *tutte* le sue condizioni **necessarie** e **sufficienti**. Possiamo includere nella sua specifica che *un POLIGONO, **per definizione** ha tre o più lati che sono SEGMENTI*

##### Concetti definiti

* TRIANGOLO è un concetto definito: *un TRIANGOLO è esattamente un POLIGONO con tre lati*
    - diventa un nuovo termine, definito da nulla di più della sua definizione. La definizione fornisce sia le condizioni **necessarie** che **sufficienti** per l'esistenza di un triangolo

#### Gerarchia di concetti

"message from pippo@pippa.pip" è una *descrizione composta* che implicitamente specializza "message"

Le **relazioni di specializzazione** implicide nelle descrizioni composte sono espresse naturalmente con **grafo diretto**

#### Classificazione

* **Sussunzione**: avviene quando un'istanza di una descrizione KL-ONE è *sempre* anche un'istanza di un'altra descrizione KL-ONE
* **classificatore**: meccanismo per prendere una nuova descrizione KL-ONE e metterla nel punto giusto della gerarchia.
    - Si trova nel posto giusto se è sotto tutte le descrizioni che la sussumono, e sopra tutte le descrizioni che sussume

### Concetti generici e tassonomia di base

#### Concetti

Il tipo più importante è il **Generic concept**, l'equivalente in KL-ONE di "termine generale".

* potenzialmente molti individui in un possibile mondo possono essere descritti da un generic concept
* es. di generic concept: *animal, mammal, human, female human, woman*. ognuno di questi èuna descrizione che può essere utilizzata per descrivere molti individui nel mondo
    - Alcuni di questi possono essere formati dagli altri (vedi "human" e "mammal")

#### Componenti di un concetto

Le componenti di un concetto sono:

* I concetti **che lo sussumono** (i suoi *superconcetti*)
* La sua struttura locale interna, espressa in:
    - **ruoli**, che descrivono le relazioni potenziali tra le istanze del concetto stesso e quelle di altri concetti associati (es: proprietà, parti ecc..)
    - **descrizioni strutturali**, che esprimono le interrelazioni tra i ruoli


## Wordnet

on-line lexical database

### Componenti principali

#### Lessico suddiviso in 4 categorie

* Nouns
* Verbs
* Adjectives
* Adverbs

#### Lexical matrix

Matrice lessicale, in cui:

* intestazione righe: **word forms**
* intestazione colonne: **word meanings**
* cella i,j: indica se la word form i può essere utilizzata per esprimere la word meaning j
* 2 o + entry nella stessa colonna: la word form è **polysemous**
* 2 o + entry nella stessa riga: le word form sono **synonyms**

Le righe sono **synonym sets** (synset)

#### Glossa (gloss)

esempio: {board, (a committee having supervisory powers; "the
board has seven members")}

serve per differenziare un significato di "board" dagli altri. Si può vedere come un synset con un singolo membro

#### Relazioni semantiche

##### Synonymy

2 espressioni sono sinonime se la sostituzione di una al posto dell'altra in un contesto linguistico non altera il valore di verità.

##### Antonymy

è una relazione *lessicale* tra **word forms** (non tra synset)

esempio: rise/fall oppure ascend/descend, ma non {rise,ascend}/{fall,descend} perchè rise/descend non sono antonimi

##### Hyponymy/Hyperonymy

è una relazione *semantica* tra **word meanings**

esempio: tree è hyponym di plant, plant è hyperonym di tree

##### Meronymy/Holonymy

è una relazione tra **synset**, di tipo *part-whole* (o HASA)

il synset {x,x'...} è un **meronym** di {y,y'...} se i madrelingua inglesi accettano frasi costruite con frame tipo "A y has an x (as a part)" o "An x is a part of y"

viceversa, {y,y'...} è **holonym** di {x,x'...}

#### Componenti semantiche

I **nomi** sono messi in una multi-gerarchia.

25 **unique beginners** sono al top della gerarchia

    {act, action, activity} {natural object}
    {animal, fauna} {natural phenomenon}
    {artifact} {person, human being}
    {attribute, property} {plant, flora}
    {body, corpus} {possession}
    {cognition, knowledge} {process}
    {communication} {quantity, amount}
    {event, happening} {relation}
    {feeling, emotion} {shape}
    {food} {state, condition}
    {group, collection} {substance}
    {location, place} {time}
    {motive}

tutti i nomi inglesi sono in relazione **hyponymic** con questi unique beginners

#### Distinguishing features

La *gerarchia* è costituita dalle relazioni di **hyponymy**. I *dettagli* sono dati da **features** che distinguono un concetto da un altro.

    a canary is a bird that is small, colorful, sings, and flies

* is a bird: **hyponymy** - colloca canary in relazione con bird nella gerarchia, da cui eredita:
    - has beak and wings: **parts**
        + espresse con *nomi*
* is small, colorful: **attributes**
    - espressi con *aggettivi*
* sings and flies: **functions**
    - espressi con *verbi*

##### Attributes (come modificatori)

Espressi con **aggettivi**. Gli aggettivi *modificano* i nomi

I **nomi** fungono da *argomento* per gli **attributi**

    Size(canary) = small

##### Parts

Collegate con la relazione di **meronymy/holonymy**

Transitività limitata:
    
    la porta ha una maniglia -> maniglia è meronym di porta
    la casa ha una porta -> porta è meronym di casa
    la casa ha una maniglia??

Diversi tipi di meronyms:

1. component-object (branch/tree)
2. member-collection (tree/forest)
3. portion-mass (slice/cake)
4. stuff-object (aluminum/airplane)
5. feature-activity (paying/shopping)
6. place-area (Princeton/New jersey)
7. phase-process (adolescence/growing up)

##### Functions (as feature of meaning)

boh

#### Verbi

##### Troponymy

la **hyponymy** non vale per i verbi. esiste la **troponymy**:

    To [verb1] is to [verb2] in some particular manner

