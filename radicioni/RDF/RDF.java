import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;

import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.*;

public class RDF {
  public final static String DOC_FOLDER = "./lib/news";
  public final static String RDF_FILE = "./lib/news_collection.rdf";
  public final static String LEMMARIO = "./lib/lemmario.txt";
  public final static String STOPWORDS = "./lib/stopwords.txt";

  // the set of documents D
  public static DocumentLibrary library = new DocumentLibrary();

  // Read all documents from file
  public static void readAllDocuments(HashSet<String> stopWords, Map<String,String[]> lemmario) throws IOException{
    Files.walk(Paths.get(DOC_FOLDER)).forEach(filePath -> {
      if (Files.isRegularFile(filePath)) {
        try{
          String content = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
          Path p = filePath.getFileName();
          final Document doc = new Document(filePath.getFileName().toString(), content);
          doc.cleanContent(stopWords);
          doc.setLemmas(lemmario);
          doc.setSubject();
          doc.setFields();
          library.add(doc);
        }
        catch(IOException ex){
          throw new RuntimeException("File not found");
        }
      }
    });
  }

  public static void main(String[] args) throws IOException{
    // Read stop words from file
    System.out.print("Reading stop words from file... ");
    HashSet<String> stopWords = NLUtil.readStopWords(STOPWORDS);
    System.out.println("ok - " + stopWords.size() + " stop words read");

    // Read lemmario from file
    System.out.print("Reading the lemmario from file... ");
    Map<String,String[]> lemmario = NLUtil.readLemmarioEnglish(LEMMARIO);
    System.out.println("ok - " + lemmario.size() + " lemmas read");

    // Read all documents
    System.out.print("Reading all documents... ");
    readAllDocuments(stopWords, lemmario);
    System.out.println("ok - " + library.size() + " documents read");


    System.out.println("Printing all documents... ");
    library.writeToConsole();
    library.writeToFile();

    System.out.println("Querying...");

    System.out.println("\n - all documents having <dc:creator=\"topo gigio\"> ");
    library.query(QueryDB.TOPOGIGIO_QUERY);

    System.out.println("\n - description of documents having <dc:title=\"Avatar breaks US DVD sales record\"> ");
    library.query(QueryDB.EXISTENTTITLE_QUERY);

    System.out.println("\n - description of documents having <dc:title=\"Test Non Esistente.Ciao.\"> ");
    library.query(QueryDB.NONEXISTENTTITLE_QUERY);

    System.out.println("\n...Finished!");
  }


  // Document Class, represent a single document along with
  // its category, filename and content
  public static class Document {
    // fields
    String title, subject = "test", description, date, creator, publisher = "BBC", uri;

    static int nDocuments = 0;

    //nome del file
    private String filename;
    // contenuto grezzo del documento
    private String content;
    // categoria del documento prelevata dal filename
    private String category;
        // contenuto pulito (dalle stop words, punteggiatura e a capi)
    private String[] cleanedContent;
    // categoria del documento prelevata dal filename
    private String[][] lemmas;
    // categoria riconosciuta dall'algoritmo
    private HashMap<String,Integer> frequency;

    public Document(String fn, String c) {
      filename = fn;
      content = c;
      frequency = new HashMap<String,Integer>();
    }

    public void setFields(){
      uri = content.replaceAll("^#\\s*#\\s*([\\w:/.-]*)[\\s\\S]*$","$1").trim();
      title = content.replaceAll("^#\\s*#\\s*[\\w:/.-]*\\s*#\\s*([\\w.,?' ]*)[\\s\\S]*$", "$1").trim();
      description = content.replaceAll("^#\\s*#\\s*[\\w:/.-]*\\s*#\\s*[\\w.,?' -]*\\s*([\\w.,?' -]*)[\\s\\S]*$", "$1").trim();
      date = content.replaceAll("[\\s\\S]*Page last updated at ([\\w :,]*)\\s*$", "$1").trim();
      if(nDocuments<2) creator = "topo gigio";
      else if(nDocuments%2==0) creator = "Ruben Caliandro";
      else creator = "Roberto Pesando";
      nDocuments++;
      // System.out.println("\n");
      // System.out.println("uri: " + uri);
      // System.out.println("title: " + title);
      // System.out.println("description: " + description);
      // System.out.println("date: " + date);
    }

    public void cleanContent(HashSet<String> stopWords){
      cleanedContent = NLUtil.cleanSentence(content, stopWords);
    }

    public void setLemmas(Map<String,String[]> lemmario){
      lemmas = NLUtil.lemmatise(cleanedContent, lemmario);
    }

    public void setSubject(){
      for (String[] lemma: lemmas) {
        Integer n = frequency.get(lemma[0]);
        if(n == null) n = 0;
        frequency.put(lemma[0], n+1);
      }
      Map.Entry<String,Integer> first = null;
      Map.Entry<String,Integer> second = null;
      Map.Entry<String,Integer> third = null;
      for (Map.Entry<String,Integer> entry : frequency.entrySet()) {
        if(first == null || entry.getValue() > first.getValue()){
          third = second;
          second = first;
          first = entry;
        }
        else if(second == null || entry.getValue() > second.getValue()){
          third = second;
          second = entry;
        }
        else if(third == null || entry.getValue() > third.getValue()){
          third = entry;
        }
      }
      subject = first.getKey() + " " + second.getKey() + " " + third.getKey();
    }
  }

  /**
   *
   */
  public static class DocumentLibrary {

    // list of documents
    // note: each document has a dictionary
    public ArrayList<Document> documents;

    // RDF Model (empty)
    private Model model;

    private OutputStream output;
    private InputStream input;

    public DocumentLibrary(){
      documents = new ArrayList<Document>();
      // create an empty model
      model = ModelFactory.createDefaultModel();
    }

    // add a document inside a library
    public void add(Document doc){
      documents.add(doc);
      addResource(doc);
    }

    public void addResource(Document doc) {
      Resource r = model.createResource("http://" + doc.uri);
      r.addProperty(DC_11.title, doc.title);
      r.addProperty(DC_11.subject, doc.subject);
      r.addProperty(DC_11.description, doc.description);
      r.addProperty(DC_11.date, doc.date);
      r.addProperty(DC_11.creator, doc.creator);
      r.addProperty(DC_11.publisher, doc.publisher);
    }

    // get the number of documents inside the library
    public int size() {
      return documents.size();
    }

    public Document get(int index) {
      return documents.get(index);
    }

    public void query(String qString) {
      Query query = QueryFactory.create(qString);

      // Execute the query and obtain results
      QueryExecution qe = QueryExecutionFactory.create(query, model);
      ResultSet results = qe.execSelect();

      ResultSetFormatter.out(System.out, results, query) ;

      // Important - free up resources used running the query
      qe.close();
    }

    public void query(QueryDB qDB) {
      query(qDB.toString());
    }

    public void writeToConsole(){
      model.write(System.out, "RDF/XML-ABBREV");
    }

    public void writeToFile() throws IOException{
      output = new FileOutputStream(RDF_FILE);
      model.write(output, "RDF/XML-ABBREV");
      output.close();
    }

    public void readFromFile() throws IOException {
      input = FileManager.get().open(RDF_FILE);

      // reset model;
      model = ModelFactory.createDefaultModel();

      // read the RDF/XML file
      model.read(input, null); // null base URI, since model URIs are absolute

      // close input stream
      input.close();
    }
  }

  public static class QueryDB {
    public static String EXAMPLE_QUERY =
      "PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
      "SELECT ?url " +
      "WHERE {" +
      "      ?contributor foaf:name \"Jon Foobar\" . " +
      "      ?contributor foaf:weblog ?url . " +
      "      }";

    // tutti i title
    public static String TITLE_QUERY =
      " PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
      " SELECT ?title "+
      " WHERE {"+
      " ?r dc:title ?title ."+
      "}";

    // la description della risorsa avente un certo title (sperimentare cosa capita con titoli esistenti e inesistenti).
    public static String EXISTENTTITLE_QUERY =
      " PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
      " SELECT ?description "+
      " WHERE {"+
      " ?r dc:title \"Avatar breaks US DVD sales record\" ."+
      " ?r dc:description ?description ."+
      "}";
    public static String NONEXISTENTTITLE_QUERY =
      " PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
        " SELECT ?description "+
        " WHERE {"+
        " ?r dc:title \"Test Non Esistente.Ciao.\" ."+
        " ?r dc:description ?description ."+
        "}";

    //  il title dei documenti che hanno come creator "topo gigio";
    public static String TOPOGIGIO_QUERY =
      "PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
      "SELECT ?title "+
      "WHERE { "+
      " ?r dc:title ?title ."+
      " ?r dc:creator \"topo gigio\" ."+
      " } ";
  }
}
