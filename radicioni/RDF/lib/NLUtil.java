import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.Arrays;
import java.util.HashSet;

import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.jlt.util.Language;

import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.POS;

public class NLUtil {
  public final static Boolean DEBUG = false;
  public final static Boolean NORMALIZE_SCORE = false;
  public final static String PUNCTUATION = "[`’,;.:«»\\\\|!\"£$%&/()=?^'\\[\\]{}/*-\\+@#§\\-_“”\\s\\r\\n]";

  private static void d(String str) {
    if (DEBUG) {
      System.out.println("[DEBUG]: "+ str);
    }
  }

  // read the input file with the stopwords
  // Read lines from file and put them into an array. Skip empty lines
  public static HashSet<String> readStopWords(String filepath){
    try(Scanner scan = new Scanner(new File(filepath)).useDelimiter("\n")){
      // Store lines in an array
      HashSet<String> stopWords = new HashSet<String>();
      while(scan.hasNext()){
        String s = scan.next().split("\\|")[0].trim();
        if(s.compareTo("") != 0)
          stopWords.add(s);
      }
      scan.close();

      return stopWords;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + filepath + " not found!");
    }
  }

  // read the lemmario from file
  public static Map<String,String[]> readLemmario(String filepath) {
    // TODO: read also the POS
    try(Scanner scan = new Scanner(new File(filepath)).useDelimiter("\n")){
      Map<String,String[]> lemmario = new HashMap<String,String[]>();
      while(scan.hasNext()){
        String[] entry = scan.next().trim().split("\t");
        if(entry.length == 3){
          String pos = entry[2].replaceAll("[:-][\\w:+]*", "");
          String[] value = {entry[1].toLowerCase(), pos};
          String[] oldvalue = lemmario.get(entry[0]);
          if (oldvalue == null || pos.compareTo("NOUN") == 0) {
            lemmario.put(entry[0].toLowerCase(), value);
          }
        }
      }
      scan.close();
      return lemmario;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + filepath + " not found!");
    }
  }

  // read the lemmario from file
  public static Map<String,String[]> readLemmarioEnglish(String filepath) {
    // TODO: read also the POS
    try(Scanner scan = new Scanner(new File(filepath)).useDelimiter("\n")){
      Map<String,String[]> lemmario = new HashMap<String,String[]>();
      while(scan.hasNext()){
        String[] entry = scan.next().trim().split("\t");
        if(entry.length == 2){
          String[] v = {entry[1].toLowerCase(), "LOL"};
          lemmario.put(entry[0].toLowerCase(), v);
        }
      }
      scan.close();
      return lemmario;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + filepath + " not found!");
    }
  }

  public static String[] cleanSentence(String sentence, HashSet<String> stopWords) {
    //lowercase
    sentence = sentence.toLowerCase();
    //tokenize
    String[] tokens = sentence.split(PUNCTUATION);
    // remove stop words
    ArrayList<String> res = new ArrayList<String>();

    for (String token : tokens) {
      token = token.trim();
      if (!stopWords.contains(token)) {
        res.add(token);
      }
    }

    return res.toArray(new String[res.size()]);
  }

  public static String[][] lemmatise(String[] tokens, Map<String,String[]> lemmario) {
    ArrayList<String[]> lemmasList = new ArrayList<String[]>();
    for (String token : tokens) {
      token = token.trim();
      if(token.compareTo("") != 0){
        String[] lemma = lemmario.get(token);
        if (lemma != null) {
          lemmasList.add(lemma);
        }
      }
    }
    return lemmasList.toArray(new String[lemmasList.size()][]);
  }

  // copied by Babelnet's source and modified, to limit the edges (otherwise it takes a quasi-infinite time!!!)
  private static Map<IPointer, List<BabelSynset>> getRelatedMap(BabelSynset synset, int maxExtension) throws IOException
  {
    // init the relation map
    BabelNet bn = BabelNet.getInstance();
    Map<IPointer,List<BabelSynset>> relatedMap = new HashMap<IPointer, List<BabelSynset>>();
    List<BabelNetGraphEdge> edges = bn.getSuccessorEdges(synset.getId());
    int i = 0;
    for (BabelNetGraphEdge e : edges)
    {
      IPointer relation = e.getPointer();
      List<BabelSynset> synsetsForRelation = relatedMap.get(relation);
      if (synsetsForRelation == null)
      {
        synsetsForRelation = new ArrayList<BabelSynset>();
        relatedMap.put(relation, synsetsForRelation);
      }
      String id = e.getTarget();
      BabelSynset relatedSynset = bn.getSynsetFromId(id);
      synsetsForRelation.add(relatedSynset);
      i++;
      if(i >= maxExtension-1) break;
    }
    return relatedMap;
  }

  public static BabelSynset[] extractRelatedSynsets(BabelSynset synset, int maxExtension) throws IOException{
    ArrayList<BabelSynset> list = new ArrayList<BabelSynset>();
    Map<IPointer,List<BabelSynset>> map = getRelatedMap(synset, maxExtension);
    list.add(synset);
    for (Map.Entry<IPointer,List<BabelSynset>> entry : map.entrySet()) {
      List<BabelSynset> curlist = entry.getValue();
      for (BabelSynset syn : curlist) {
        list.add(syn);
      }
    }

    return list.toArray(new BabelSynset[list.size()]);
  }

  // get the extended gloss of the synset
  public static String[] extractGlosses(BabelSynset[] synsets) throws IOException{
    // use Babelnet to extract the gloss of the synset and the gloss of all related synsets
    ArrayList<String> result = new ArrayList<String>();

    for (BabelSynset synset : synsets) {
      List<BabelGloss> bblGlosses = synset.getGlosses(Language.IT);

      // debug glosses size
      d("synset_id: " + synset.getId() + " (" + bblGlosses.size() + " glosses)");
      for (BabelGloss gloss : bblGlosses) {
        // debug gloss string
        d("- "+ gloss.getGloss());
        result.add(gloss.getGloss());
      }
    }
    return result.toArray(new String[result.size()]);
  }

  // Return the overlap between the context and the gloss
  public static float computeOverlap(String[][] context, String[][] gloss, String word){
    float score = 0;
    for (String[] c : context) {
      for (String[] g : gloss) {
        if(c[0].compareTo(g[0]) == 0 && c[0].compareTo(word) != 0 && g[0].compareTo(word) != 0){
          score++;
          System.out.print(g[0] + " ");
        }
      }
    }
    if(NORMALIZE_SCORE) score = score / gloss.length;
    return score;
  }

  // public static BabelSynset extendedLesk(String word, String contextSentence, String[] stopWords, Map<String,String[]> lemmario, int maxExtension) throws IOException{
  //   BabelNet bn = BabelNet.getInstance();

  //   // Clean the context
  //   contextSentence = cleanSentence(contextSentence, stopWords);

  //   // Lemmatise the context (use the lemmario)
  //   String[][] context = lemmatise(contextSentence, lemmario);

  //   // Get all the synsets for this word
  //   System.out.print("Retrieving all the synsets... ");
  //   List<BabelSynset> synsets = bn.getSynsets(Language.IT, word, POS.NOUN);
  //   System.out.println("ok - retrieved " + synsets.size() + " synsets");

  //   float maxScore = -1;
  //   BabelSynset bestSynset = null;
  //   String bestGloss = null;

  //   System.out.println("Running extended Lesk... ");
  //   int i=0;
  //   // For each synset
  //   for (BabelSynset synset: synsets) {
  //     System.out.println("  synset " + i + "\n    main sense: " + synset.getMainSense());
  //     i++;
  //     d("Processing synset " + synset.getMainSense());

  //     // Get the related synsets and all the glosses
  //     BabelSynset[] relatedSynsets = extractRelatedSynsets(synset, maxExtension);
  //     String[] glosses = extractGlosses(relatedSynsets);
  //     System.out.println("    extended synsets: " + relatedSynsets.length);
  //     for (BabelSynset s : relatedSynsets) {
  //       d(s.getMainSense());
  //     }

  //     float bestPartial = -1;
  //     String bestPartialGloss = null;
      

  //     float score = 0;
  //     System.out.print("    matched: ");
  //     // skip if zero glosses returned
  //     if (glosses.length > 0) {
  //       // For each gloss of the extended glosses array
  //       for (String glossString : glosses) {
  //         String[][] gloss = lemmatise(cleanSentence(glossString, stopWords), lemmario);

  //         // Compute the overlap between the context and the gloss
  //         float partial = computeOverlap(context, gloss, word);
  //         d("partial score: " + partial);
          
  //         if(partial > bestPartial){
  //           bestPartial = partial;
  //           bestPartialGloss = glossString;
  //         }

  //         score += partial;
  //       }

  //       // Update maxScore and bestSynset
  //       if (score >= maxScore) {
  //         maxScore = score;
  //         bestSynset = synset;
  //         bestGloss = bestPartialGloss;
  //       }
  //     }
  //     System.out.println("\n    score: " + score);
  //   }
  //   System.out.println("Best sense found for word " + word + ": " + bestSynset.getMainSense());
  //   System.out.println("Best gloss found: " + bestGloss);
  //   System.out.println("with score: " + maxScore);
  //   return bestSynset;
  // }
}
