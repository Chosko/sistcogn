#!/bin/bash
java -mx2g -classpath $CLASSPATH:./lib/apache-jena-2.11.2/lib/*.jar:./lib/apache-jena-2.11.2/lib/log4j-1.2.17.jar:./lib/apache-jena-2.11.2/lib-src/*.jar:../extended_lesk/lib/BabelNet-API-2.5/*.jar:../extended_lesk/lib/BabelNet-API-2.5/bin:../extended_lesk/lib/BabelNet-API-2.5/lib/*:../extended_lesk/lib/BabelNet-API-2.5/config/*:./lib/ RDF
