#!/bin/bash
javac -classpath $CLASSPATH:../extended_lesk/lib/BabelNet-API-2.5/*.jar:../extended_lesk/lib/BabelNet-API-2.5/bin:../extended_lesk/lib/BabelNet-API-2.5/lib/*:../extended_lesk/lib/BabelNet-API-2.5/config/* ./lib/NLUtil.java
javac ./lib/Progressbar.java
javac -classpath $CLASSPATH:./lib/apache-jena-2.11.2/lib/*.jar:../extended_lesk/lib/BabelNet-API-2.5/*.jar:../extended_lesk/lib/BabelNet-API-2.5/bin:../extended_lesk/lib/BabelNet-API-2.5/lib/*:../extended_lesk/lib/BabelNet-API-2.5/config/*:./lib/ ./RDF.java
