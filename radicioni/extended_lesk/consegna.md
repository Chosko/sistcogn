# WSD in italiano - extended gloss overlap - Babelnet

## Gloss overlap (Lesk algorithm)

Un approccio semplice e intuitivo basato sulla conoscenza è *calcolare l'overlap* tra le definizioni dei sensi di due o più parole (lesk algorithm).

* Dato un contesto di due parole (w_1,w_2), si assume che i sensi delle parole le cui definizioni hanno il maggiore overlap (cioè le parole in comune) siano quelli corretti.
* Formalmente, date due parole w_1 e w_2, viene calcolato il seguente punteggio (score) per ogni coppia di sensi S_1 appartenente a Senses(w_1) e S_2 appartentente a Senses(w_2):
    - **score_lesk(S_1,S_2)** = overlap(gloss(S_1),gloss(S_2))
    - dove gloss(S_i) è la bag of words nella definizione testuale del senso S_i di w_i.
* I sensi che massimizzano questo punteggio vengono assegnati alle rispettive parole

Se estendiamo l'algoritmo a un contesto di n parole, dobbiamo determinare tutte le possibili combinazioni di overlap (il numero di combinazioni è dato dalla produttoria del numero di sensi di ogni parola del contesto)

* Dato il numero esponenziale di passi richiesti, si impiega una variante del Lesk che identifica il senso di una parola w la cui definizione testuale ha il più alto overlap con le parole del contesto di w
* Formalmente, data una parola target w, viene calcolato il seguente punteggio per ogni senso S di w:
    - **score_leskvar(S)** = overlap(context(w),gloss(S))

## Problemi

* Sfortunatamente, l'approccio Lesk è molto sensibile alle parole esatte nelle definizioni, quindi l'assenza di una certa parola può cambiare radicalmente i risultati.
* L'algoritmo determina gli overlap solo per le glosse dei sensi che sono stati considerati
    - Questa è una limitazione significativa perchè le glosse nei dizionari tendono a essere molto corte e non forniscono un vocabolario sufficiente per avere una distinzione fine tra i sensi

## Extended gloss overlap

L'idea è di espandere le glosse delle parole che devono essere confrontate per includere le glosse dei concetti che si sa che sono relazionati attraverso esplicite nel dizionario (per esempio iperonimia, meronimia ecc..) e con BabelNet (redirezioni, traduzioni, glosse)

Per ogni senso S di una target word w, si stima il suo punteggio come:

* **score_extlesk(S)** = sommatoria(overlap(context(w),gloss(S')))
    - dove context(w) è il bag di tutte le parole all'interno di una context window attorno alla target word w
    - dove gloss(S') è il bag di parole nella definizione di un senso S'
    - dove S' è un senso relazionato con S oppure S stesso.

## Architettura del sistema

    <frase,termine>
           |
           |
    lemmatizzazione  ----------> Lemmario
           |
           |
    espansione del
    contesto di      ----------> BabelNet
    disambiguazione
    (lemmatizzazione anche qui)
           |
           |
    algoritmo di Lesk
    con extended
    gloss overlap
           |
           |
    senso del termine

## Parole da disambiguare, nei loro contesti

* termine polisemico 'pianta'
    - La pianta dell'alloggio è disponibile in ufficio, accanto all'appartamento; dal disegno è possibile cogliere i dettagli dell'architettura dello stabile, sulla distribuzione dei vani e la disposizione di porte e finestre.
    - I platani sono piante ad alto fusto, organismi viventi: non ha senso toglierli per fare posto a un parcheggio.
* termine polisemico 'testa'
    - Si tratta di un uomo facilmente riconoscibile: ha una testa piccola, gli occhi sporgenti, naso adunco e piccole orecchie a sventola.
    - Come per tutte le cose, ci vorrebbe un po' di testa, un minimo di ragione, una punta di cervello, per non prendere decisioni fuori dal senso dell'intelletto


# Librerie

Per far funzionare il progetto, scaricare le seguenti librerie:

* [BabelNet API 2.5](http://babelnet.org/data/2.5/BabelNet-API-2.5.tar.bz2)
* [BabelNet 2.5 index bundle](http://babelnet.org/data/2.5/babelnet-2.5-index-bundle.tar.bz2)

decomprimerle ed inserirle nella cartella lib, facendo attenzione a non sostituire la cartella BabelNet-API-2.5/config
