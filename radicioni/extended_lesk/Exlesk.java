import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.Arrays;

import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.jlt.util.Language;

import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.POS;

public class Exlesk {
  public final static String INPUTFILE = "./lib/frasi.txt";
  public final static String STOPWORDS = "./lib/stopwords.txt";
  public final static String LEMMARIO = "./lib/lemmario.txt";
  public final static String BABELNET_CONFIG = "./lib/BabelNet-API-2.5/config/babelnet.properties";
  public final static Boolean DEBUG = false;
  public final static int MAX_EXTENSION = 50;
  public final static Boolean NORMALIZE_SCORE = false;

  // Stop words
  public static String[] stopWords;

  // Lemmario
  public static Map<String,String[]> lemmario;

  public static void d(String str) {
    if (DEBUG) {
      System.out.println("[DEBUG]: "+ str);
    }
  }

  // read the input file with the stopwords
  // Read lines from file and put them into an array. Skip empty lines
  public static String[] readStopWords(){
    try(Scanner scan = new Scanner(new File(STOPWORDS)).useDelimiter("\n")){
      // Store lines in an array
      String[] lines;
      ArrayList<String> list = new ArrayList<String>();
      while(scan.hasNext()){
        String s = scan.next().split("\\|")[0].trim();
        if(s.compareTo("") != 0)
          list.add(s);
      }
      scan.close();

      lines = list.toArray(new String[list.size()]);
      return lines;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + STOPWORDS + " not found!");
    }
  }

  // read the input file with the sentences to disambiguate
  public static String[][] readInput() {
    try(Scanner scan = new Scanner(new File(INPUTFILE)).useDelimiter("\n")){
      ArrayList<String[]> inputList = new ArrayList<String[]>();
      String[] curSentence = null;
      while(scan.hasNext()){
        String s = scan.next().trim();
        if(s.compareTo("") != 0){
          if (s.startsWith("#")) {
            s = s.substring(1).trim();
            curSentence = new String[2];
            curSentence[0] = s;
            curSentence[1] = "";
            inputList.add(curSentence);
          }
          else{
            if(curSentence == null){
              throw new RuntimeException("Syntax error in " + INPUTFILE + "!");
            }
            else{
              curSentence[1] = curSentence[1] + " " + s;
            }
          }
        }
      }
      scan.close();
      return inputList.toArray(new String[inputList.size()][]);
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + INPUTFILE + " not found!");
    }
  }

  // read the lemmario from file
  public static Map<String,String[]> readLemmario() {
    // TODO: read also the POS
    try(Scanner scan = new Scanner(new File(LEMMARIO)).useDelimiter("\n")){
      Map<String,String[]> lemmario = new HashMap<String,String[]>();
      while(scan.hasNext()){
        String[] entry = scan.next().trim().split("\t");
        if(entry.length == 3){
          String pos = entry[2].replaceAll("[:-][\\w:+]*", "");
          String[] value = {entry[1], pos};
          String[] oldvalue = lemmario.get(entry[0]);
          if (oldvalue == null || pos.compareTo("NOUN") == 0) {
            lemmario.put(entry[0], value);
          }
        }
      }
      scan.close();
      return lemmario;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + LEMMARIO + " not found!");
    }
  }

  public static String cleanSentence(String sentence) {
    //lowercase
    sentence = sentence.toLowerCase();
    // remove punctuation
    sentence = sentence.replaceAll("\\s+po'\\s+", " ");
    // remove punctuation
    sentence = sentence.replaceAll("[.,;!?:']", " ");
    // remove multiple whitespaces
    sentence = sentence.replaceAll("\\s+", " ");
    // remove multiple whitespaces
    sentence = sentence.replaceAll("\\t+", " ");
    // remove stop words
    for (String sw : stopWords) {
      sentence = sentence.replaceAll("\\s+" + sw + "\\s+", " ");
    }

    return sentence;
  }

  // lemmatise a sentence
  public static String[][] lemmatise(String sentence) {
    String[] tokens = sentence.split(" ");
    ArrayList<String[]> lemmasList = new ArrayList<String[]>();
    for (String token : tokens) {
      token = token.trim();
      if(token.compareTo("") != 0){
        String[] lemma = lemmario.get(token);
        if (lemma == null) {
          lemma = new String[2];
          lemma[0] = token;
          lemma[1] = "UNKNOWN";
        }
        lemmasList.add(lemma);
      }
    }
    return lemmasList.toArray(new String[lemmasList.size()][]);
  }

  // copied by Babelnet's source and modified, to limit the edges (otherwise it takes a quasi-infinite time!!!)
  public static Map<IPointer, List<BabelSynset>> getRelatedMap(BabelSynset synset) throws IOException
  {
    // init the relation map
    BabelNet bn = BabelNet.getInstance();
    Map<IPointer,List<BabelSynset>> relatedMap = new HashMap<IPointer, List<BabelSynset>>();
    List<BabelNetGraphEdge> edges = bn.getSuccessorEdges(synset.getId());
    int i = 0;
    for (BabelNetGraphEdge e : edges)
    {
      IPointer relation = e.getPointer();
      List<BabelSynset> synsetsForRelation = relatedMap.get(relation);
      if (synsetsForRelation == null)
      {
        synsetsForRelation = new ArrayList<BabelSynset>();
        relatedMap.put(relation, synsetsForRelation);
      }
      String id = e.getTarget();
      BabelSynset relatedSynset = bn.getSynsetFromId(id);
      synsetsForRelation.add(relatedSynset);
      i++;
      if(i >= MAX_EXTENSION-1) break;
    }
    return relatedMap;
  }

  public static BabelSynset[] extractRelatedSynsets(BabelSynset synset) throws IOException{
    ArrayList<BabelSynset> list = new ArrayList<BabelSynset>();
    Map<IPointer,List<BabelSynset>> map = getRelatedMap(synset);
    list.add(synset);
    for (Map.Entry<IPointer,List<BabelSynset>> entry : map.entrySet()) {
      List<BabelSynset> curlist = entry.getValue();
      for (BabelSynset syn : curlist) {
        list.add(syn);
      }
    }

    return list.toArray(new BabelSynset[list.size()]);
  }

  // get the extended gloss of the synset
  public static String[] extractGlosses(BabelSynset[] synsets) throws IOException{
    // use Babelnet to extract the gloss of the synset and the gloss of all related synsets
    ArrayList<String> result = new ArrayList<String>();

    for (BabelSynset synset : synsets) {
      List<BabelGloss> bblGlosses = synset.getGlosses(Language.IT);

      // debug glosses size
      d("synset_id: " + synset.getId() + " (" + bblGlosses.size() + " glosses)");
      for (BabelGloss gloss : bblGlosses) {
        // debug gloss string
        d("- "+ gloss.getGloss());
        result.add(gloss.getGloss());
      }
    }
    return result.toArray(new String[result.size()]);
  }

  // Return the overlap between the context and the gloss
  public static float computeOverlap(String[][] context, String[][] gloss, String word){
    float score = 0;
    for (String[] c : context) {
      for (String[] g : gloss) {
        if(c[0].compareTo(g[0]) == 0 && c[0].compareTo(word) != 0 && g[0].compareTo(word) != 0){
          score++;
          System.out.print(g[0] + " ");
        }
      }
    }
    if(NORMALIZE_SCORE) score = score / gloss.length;
    return score;
  }

  public static void main(String[] args) throws IOException{
    System.out.println("Loading BabelNet...");
    BabelNetConfiguration bc = BabelNetConfiguration.getInstance();
    bc.setConfigurationFile(new File(BABELNET_CONFIG));
    BabelNet bn = BabelNet.getInstance();
    System.out.println();
    // bn.getSynsets(Language.IT, "testa", POS.NOUN).get(2).getRelatedMap();

    // Read sentences and target words form file
    System.out.print("Reading input sentences from file... ");
    String[][] input = readInput();
    System.out.println("ok");

    // Read the lemmario from file
    System.out.print("Reading the lemmario from file... ");
    lemmario = readLemmario(); //Key: word, Value: array [lemma,POS]
    System.out.println("ok - " + lemmario.size() + " lemmas read");

    // read stop words
    System.out.print("Reading stop words from file... ");
    stopWords = readStopWords();
    System.out.println("ok - " + stopWords.length + " stopwords read");

    // clone input allowing to get the original sentence
    // TODO: a more efficient way to do it
    ArrayList<String[]> cleanedInputList = new ArrayList<String[]>();
    for (String[] in: input) {
      cleanedInputList.add(in.clone());
    }
    String[][] cleanedInput = cleanedInputList.toArray(new String[cleanedInputList.size()][]);


    // clean the sentences
    System.out.print("Cleaning the input sentences...\n");
    for (int i = 0; i < input.length; i++) {
      cleanedInput[i][1] = cleanSentence(input[i][1]);
    }

    // Foreach pair word/context
    int sentenceIdx = 0;
    for (String[] inputPair : cleanedInput) {

      String w = inputPair[0];                 // The target word
      String contextSentence = inputPair[1];   // The initial context of the word
      System.out.println("\n--------------------\ntarget word: " + w);

      // printing original sentence
      System.out.println("\nsentence: "+ input[sentenceIdx][1]);

      // Lemmatise the context (use the lemmario)
      String[][] context = lemmatise(contextSentence);
      System.out.print("context: ");
      for (String[] c : context) {
        System.out.print(c[0] + " ");
      }
      System.out.println();

      // Get all the synsets for this word
      System.out.print("Retrieving all the synsets... ");
      List<BabelSynset> synsets = bn.getSynsets(Language.IT, w, POS.NOUN);
      System.out.println("ok - retrieved " + synsets.size() + " synsets");

      float maxScore = -1;
      BabelSynset bestSynset = null;
      String bestGloss = null;

      System.out.println("Running extended Lesk... ");
      int i=0;
      // For each synset
      for (BabelSynset synset: synsets) {
        System.out.println("  synset " + i + "\n    main sense: " + synset.getMainSense());
        i++;
        d("Processing synset " + synset.getMainSense());

        // Get the related synsets and all the glosses
        BabelSynset[] relatedSynsets = extractRelatedSynsets(synset);
        String[] glosses = extractGlosses(relatedSynsets);
        System.out.println("    extended synsets: " + relatedSynsets.length);
        for (BabelSynset s : relatedSynsets) {
          d(s.getMainSense());
        }

        float bestPartial = -1;
        String bestPartialGloss = null;
        

        float score = 0;
        System.out.print("    matched: ");
        // skip if zero glosses returned
        if (glosses.length > 0) {
          // For each gloss of the extended glosses array
          for (String glossString : glosses) {
            String[][] gloss = lemmatise(cleanSentence(glossString));

            // Compute the overlap between the context and the gloss
            float partial = computeOverlap(context, gloss, w);
            d("partial score: " + partial);
            
            if(partial > bestPartial){
              bestPartial = partial;
              bestPartialGloss = glossString;
            }

            score += partial;
          }

          // Update maxScore and bestSynset
          if (score >= maxScore) {
            maxScore = score;
            bestSynset = synset;
            bestGloss = bestPartialGloss;
          }
        }
        System.out.println("\n    score: " + score);
      }
      System.out.println("Best sense found for word " + w + ": " + bestSynset.getMainSense());
      System.out.println("Best gloss found: " + bestGloss);
      System.out.println("with score: " + maxScore);

      sentenceIdx++;
    }

    // prove varie per capire il funzionamento di BabelNet dato che la documentazione si limita a dire "ritorna x". Grazie.
    // d("==============");
    // List<BabelSynset> synsets = bn.getSynsets(Language.IT, "testa", POS.NOUN, true);
    // d("extracted "+ synsets.size() +" synsets");
    // for (BabelSynset synset: synsets){
    //   List<BabelGloss> bg = synset.getGlosses();
    //   for (BabelGloss gloss: bg) {
    //     d(gloss.getGloss());
    //   }
    // }
  }
}
