# Word Sense Disambiguation

Scrivere un programma che implementi l’algoritmo di Lesk per la WSD. Il
programma deve permettere la disambiguazione dei termini polisemici utilizzando
la libreria RiTa (o JAWS, o altra equivalente). Per esempio, date le due frasi:

* The house was burnt to ashes while the owner returned.
* This table is made of ash wood.

e il termine polisemico 'ash', si vuole ottenere la disambiguazione del
significato (cenere, frassino) con cui il termine occorre nella prima e nella
seconda frase.

## Primo esercizio

Il programma dovrà:

* preprocessare l'input, compiendo i seguenti passi:
    - filtraggio delle stop words, reperibili da un elenco in uno dei file disponibili fra il materiale dell’esercitazione (sperimentare come cambia il comportamento del programma con elenchi diversi).
    - stemmatizzazione: individuazione delle radici dei termini: per es. ashes -> ash con l'ausilio dell'accesso a WordNet e del metodo getStems() dell'oggetto RiWordnet;
* accedendo a WordNet, reperire i possibili sensi del termine indicato; per ciascuno dei sensi calcolare il punteggio di overlap (fra il senso corrente e la frase in cui il termine polisemico occorre) per mezzo dell'algoritmo di Lesk;
* determinare il senso con cui occorre il termine indicato nella frase corrente
(la prima e la seconda, nell'esempio proposto). Tale senso sarà determinato
come quello che massimizza il punteggio di overlap.

## Secondo esercizio

Utilizzando le API per lo [Stanford CoreNLP](http://nlp.stanford.edu/software/corenlp.shtml) sostituire la stemmatizzazione dell’esercizio
precedente, sostituendo la stemmatizzazione con la lemmatizzazione. La
lemmatizzazione consistente nell’individuare il lemma (cioè la forma canonica) dei termini nelle frasi in input.

L’utilizzo dello Stanford CoreNLP permetterà di accedere a WordNet indicando sia il POS (tendenzialmente, nei casi considerati, il pos è noun) sia il lemma dei termini.