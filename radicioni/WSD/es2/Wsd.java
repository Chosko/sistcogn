import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringReader;
import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.List;
import java.util.Arrays;

import rita.*;

import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.process.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class Wsd{
  // Path of the stop words file
  public final static String STOPWORDS_PATH = "../stop_words/stop_words_FULL.txt";

  // Path of the sentences to be disambiguated
  public final static String SENTENCES_PATH = "../sentences/sentences.txt";

  // wordnet 3.1 taken from RiTa website
  // http://rednoise.org/rita-archive/WordNet-3.1.zip
  // why 3.1 when princeton gives version 3.0? dunno
  public final static String WORDNET31 = "../lib/WordNet-3.1";

  // original wordnet 3.0 taken from princeton website
  // http://wordnetcode.princeton.edu/3.0/WordNet-3.0.tar.gz
  public final static String WORDNET30 = "../lib/WordNet-3.0";

  public final static Boolean DEBUG = false;


  // Wordnet Library
  public static RiWordNet wordnet = new RiWordNet(WORDNET31);

  // Stop words
  public static String[] stopWords;

  // Lemming stuff
  public static LexicalizedParser lp = LexicalizedParser.loadModel();
  public static TreebankLanguagePack tlp = lp.getOp().langpack();
  public static GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();

  public static void d(String str) {
    if (DEBUG) {
      System.out.println("[DEBUG]: "+ str);
    }
  }

  // Read lines from file and put them into an array. Skip empty lines
  public static String[] readLinesFromFile(String filepath){
    try(Scanner scan = new Scanner(new File(filepath)).useDelimiter("\n")){
      // Store lines in an array
      String[] lines;
      ArrayList<String> list = new ArrayList<String>();
      while(scan.hasNext()){
        String s = scan.next().trim();
        if(s.compareTo("") != 0)
          list.add(s);
      }
      scan.close();
      lines = list.toArray(new String[list.size()]);
      return lines;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + filepath + " not found!");
    }
  }

  public static String simplifiedLesk(String word, String[] words) {
    // best match in the senses dictionary
    int maxOverlap = -1;
    // local variable to save the current overlap for each sense
    int currentOverlap = maxOverlap;
    // the best matching senseId
    int maxOverlappingSenseId = -1;
    // context, the lemmatized stem sentence
    String[] context = words;
    // signature, the lemmatized glosse sentence
    String[] signature;

    String[] postags = RiTa.getPosTags(word, true);

    //d("Word: " + word);
    // for each POS get the senses for that word
    for (String postag : postags) {
      //d("- POS: " + postag);
      // get the senses (che
      int[] sensesIds = (postag.compareTo("-") != 0) ? wordnet.getSenseIds(word, postag) : new int[0];
      //d("-- senses: " + sensesIds.length);
      if (sensesIds.length > 1) { // needs disambiguation
        for (int senseId : sensesIds) {
          // get the signature == set of words in the gloss and examples of sense
          signature = getSignature(senseId);
          // computer overlapping words between signature and context
          currentOverlap = computeOverlap(word, signature, context);
          // update overlap and senseId
          if (currentOverlap > maxOverlap) {
            maxOverlappingSenseId = senseId;
            maxOverlap = currentOverlap;
          }
        }
      }
    }
    System.out.println("___");
    System.out.println("  V best overlap is "+ maxOverlap);

    if (maxOverlappingSenseId == -1)
      return null; // Return the input word for words like "the"
    else
      return wordnet.getGloss(maxOverlappingSenseId); // return the gloss if a sense was found
  }

  // enhance with wordnet.getExample?
  public static String[] getSignature(int senseId) {
    ArrayList<String> list = new ArrayList<String>();
    String[] lemmasByGlosses = getLemArray(wordnet.getGloss(senseId));
    for (String lemma: lemmasByGlosses) {
      //d(lemma);
      list.add(lemma);
    }

    String[] examples = wordnet.getExamples(senseId);
    for (String ex: examples) {
      String[] lemmasByExamples = getLemArray(ex);
      for (String lemma: lemmasByExamples) {
        //d(lemma);
        //list.add(lemma);
      }
    }
    return list.toArray(new String[list.size()]);
  }

  /**
   *
   * @param signature
   * @param context
   * @return
   */
  public static int computeOverlap(String word, String[] signature, String[] context) {
    if (DEBUG) {
      System.out.print("Signature: ");
      for (String s : signature) {
        System.out.print(s + " ");
      }
      System.out.println();
      System.out.print("Context: ");
      for (String c : context) {
        System.out.print(c + " ");
      }
      System.out.println();
    }
    int overlap = 0;
    for (String s : signature) {
      for (String c : context) {
        if(s.compareTo(c) == 0 && s.compareTo(word) != 0)
          overlap++;
      }
    }
    return overlap;
  }


  /**
   * Split and Lemmatize a sentence
   * @param sentence
   * @return
   */
  public static String[] getLemArray(String sentence) {
      Tokenizer<? extends HasWord> toke = tlp.getTokenizerFactory().getTokenizer(new StringReader(sentence));
      List<? extends HasWord> sentence2 = toke.tokenize();
      Tree parse = lp.parse(sentence2);

      ArrayList<String> lemmasList = new ArrayList<String>();

      for (LabeledWord word: parse.labeledYield()) {
        WordLemmaTag wlt = Morphology.lemmatizeStatic(new WordTag(word.value(), word.tag().toString()));
        String lemma = wlt.lemma().toString();
        if (cleanWord(lemma).length() > 0) {
          d(wlt.lemma());
          lemmasList.add(wlt.lemma().toString());
        }
      }
      
      return lemmasList.toArray(new String[lemmasList.size()]);
  }

  public static String cleanWord(String word){
    // remove stop words
    for (String sw : stopWords) {
      if(sw.compareTo(word) == 0)
       return "";
    }
    return word;
  }

  // Concatenate 2 arrays
  public static String[] concatenate (String[] A, String[] B) {
      int aLen = A.length;
      int bLen = B.length;

      @SuppressWarnings("unchecked")
      String[] C = new String[aLen+bLen];
      System.arraycopy(A, 0, C, 0, aLen);
      System.arraycopy(B, 0, C, aLen, bLen);

      return C;
  }

  public static void main(String[] args){
    // This does not make any sense. It serves to let RiTa write his fucking version message at the beginning of the execution
    RiTa.stem("");

    // Load sentences from file
    System.out.print("Loading sentences from file... ");
    String[] sentences = readLinesFromFile(SENTENCES_PATH);
    System.out.println("ok");

    // Load stop words from file
    System.out.print("Loading stop words from file... ");
    stopWords = readLinesFromFile(STOPWORDS_PATH);
    String[] punctuation = {".",",",";","'","\"","{","}","[","]","!","?"," ","''","``","\"\""};
    stopWords = concatenate(stopWords, punctuation);
    System.out.println("ok");

    for (String sentence : sentences) {
      System.out.println("\nProcessing sentence: " + sentence);

      // Clean and tokenize the sentence
      System.out.print("Cleaning sentence: ");
      //sentence= cleanWord\(sentence);
      System.out.println(sentence);

      // Lemming sentence
      System.out.println("Lemming sentence...");
      String [] lemmasArray = getLemArray(sentence);
      System.out.print("lem list: ");
      for (String lem : lemmasArray) {
        System.out.print(lem + " ");
      }
      System.out.println();

      // Lesk
      System.out.println("Disambiguating senses of words:");
      for (String lemma : lemmasArray) {
        String bestSense = simplifiedLesk(lemma, lemmasArray);
        System.out.print("- " + lemma + " // ");
        if (bestSense != null) {
          System.out.println("best sense: " + bestSense);
        }
        else {
          System.out.println("no need disambiguation");
        }
      }
    }
  }
}
