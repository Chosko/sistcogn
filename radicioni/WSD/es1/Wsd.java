import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import rita.*;

public class Wsd{
  // Path of the stop words file
  public final static String STOPWORDS_PATH = "../stop_words/stop_words_FULL.txt";

  // Path of the sentences to be disambiguated
  public final static String SENTENCES_PATH = "../sentences/sentences.txt";

  // wordnet 3.1 taken from RiTa website
  // http://rednoise.org/rita-archive/WordNet-3.1.zip
  // why 3.1 when princeton gives version 3.0? dunno
  public final static String WORDNET31 = "../lib/WordNet-3.1";

  // original wordnet 3.0 taken from princeton website
  // http://wordnetcode.princeton.edu/3.0/WordNet-3.0.tar.gz
  public final static String WORDNET30 = "../lib/WordNet-3.0";

  public final static Boolean DEBUG = true;


  // Wordnet Library
  public static RiWordNet wordnet = new RiWordNet(WORDNET31);

  // Stop words
  public static String[] stopWords;

  public static void d(String str) {
    if (DEBUG) {
      System.out.println("[DEBUG]: "+ str);
    }
  }

  // Read lines from file and put them into an array. Skip empty lines
  public static String[] readLinesFromFile(String filepath){
    try(Scanner scan = new Scanner(new File(filepath)).useDelimiter("\n")){
      // Store lines in an array
      String[] lines;
      ArrayList<String> list = new ArrayList<String>();
      while(scan.hasNext()){
        String s = scan.next().trim();
        if(s.compareTo("") != 0)
          list.add(s);
      }
      scan.close();
      lines = list.toArray(new String[list.size()]);
      return lines;
    }
    catch(FileNotFoundException e){
      throw new RuntimeException("File " + filepath + " not found!");
    }
  }

  public static String simplifiedLesk(String word, String[] words) {
    // best match in the senses dictionary
    int maxOverlap = -1;
    // local variable to save the current overlap for each sense
    int currentOverlap = maxOverlap;
    // the best matching senseId
    int maxOverlappingSenseId = -1;
    // context, the stemmatized sentence
    String[] context = getStemArray(words);
    // signature, the stemmatized glosse sentence
    String[] signature;

    String[] postags = RiTa.getPosTags(word, true);

    d("Word: " + word);
    // for each POS get the senses for that word
    for (String postag : postags) {
      d("- POS: " + postag);
      // get the senses (che
      int[] sensesIds = (postag.compareTo("-") != 0) ? wordnet.getSenseIds(word, postag) : new int[0];
      d("-- senses: " + sensesIds.length);
      if (sensesIds.length > 1) { // needs disambiguation
        for (int senseId : sensesIds) {
          // get the signature == set of words in the gloss and examples of sense
          signature = getSignature(senseId);
          // computer overlapping words between signature and context
          currentOverlap = computeOverlap(signature, context);
          // update overlap and senseId
          if (currentOverlap > maxOverlap) {
            maxOverlappingSenseId = senseId;
            maxOverlap = currentOverlap;
          }

        }
      }
    }

    if (maxOverlappingSenseId == -1)
      return null; // Return the input word for words like "the"
    else
      return wordnet.getGloss(maxOverlappingSenseId); // return the gloss if a sense was found
  }

  // enhance with wordnet.getExample?
  public static String[] getSignature(int senseId) {
    ArrayList<String> list = new ArrayList<String>();
    String[] stemsByGlosses = getStemArray(cleanSentence(wordnet.getGloss(senseId)));
    for (String stem: stemsByGlosses) {
      //d(stem);
      list.add(stem);
    }

    String[] examples = wordnet.getExamples(senseId);
    for (String ex: examples) {
      String[] stemsByExamples = getStemArray(cleanSentence(ex));
      for (String stem: stemsByExamples) {
        //d(stem);
        list.add(stem);
      }
    }
    return list.toArray(new String[list.size()]);
  }

  /**
   *
   * @param signature
   * @param context
   * @return
   */
  public static int computeOverlap(String[] signature, String[] context) {
    int overlap = 0;
    for (String s : signature) {
      for (String c : context) {
        if(s.compareTo(c) == 0)
          overlap++;
      }
    }
    return overlap;
  }


  /**
   * Split and Stemmatize a sentence
   * @param sentence
   * @return
   */
  public static String[] getStemArray(String[] words) {

    String[] stems, postags;
    ArrayList<String> stemsList = new ArrayList<String>();

    // System.out.println("[DEBUG STEM START]");
    // stemming for all POS tags
    for (String word: words) {
      // System.out.println("word: "+word);
      // get POS tags in wordnet syntax (v,a,n)
      postags = RiTa.getPosTags(word, true);
      for (String postag: postags) {
        // don't count malformed POS tags, es The.
        if (postag.compareTo("-") != 0) {
          // System.out.println("- pos: "+ postag);
          // get stems for current pos tag
          stems = wordnet.getStems(word, postag);
          // System.out.print("- stems: ");
          for (String stem: stems) {
            // System.out.print(stem + ", ");
            // adding stem
            stemsList.add(stem);
          }
          // System.out.println();
        }

      }
    }
    // System.out.println("[DEBUG STEM END]");
    return stemsList.toArray(new String[stemsList.size()]);
  }

  public static String[] cleanSentence(String sentence){
    // remove punctuation
    sentence = sentence.replaceAll("[.,;!?]", " ");
    // remove multiple whitespaces
    sentence = sentence.replaceAll("\\s+", " ");
    // remove stop words
    for (String sw : stopWords) {
      sentence = sentence.replaceAll("\\s" + sw + "\\s", " ");
    }

    return sentence.split(" ");
  }

  public static void main(String[] args){
    // This does not make any sense. It serves to let RiTa write his fucking version message at the beginning of the execution
    RiTa.stem("");

    // Load sentences from file
    System.out.print("Loading sentences from file... ");
    String[] sentences = readLinesFromFile(SENTENCES_PATH);
    System.out.println("ok");

    // Load stop words from file
    System.out.print("Loading stop words from file... ");
    stopWords = readLinesFromFile(STOPWORDS_PATH);
    System.out.println("ok");

    for (String sentence : sentences) {
      System.out.println("\nProcessing sentence: " + sentence);

      // Clean and tokenize the sentence
      System.out.print("Cleaning sentence: ");
      String[] words = cleanSentence(sentence);
      for (String word : words) {
        System.out.print(word + " ");
      }
      System.out.println();

      // stem test
      System.out.println("Stemming sentence...");
      String[] stems = getStemArray(words);
      System.out.print("stem list: ");
      for (String stem : stems) {
        System.out.print(stem + " ");
      }
      System.out.println();

      // Lesk
      System.out.println("Disambiguating senses of words:");
      for (String stem : stems) {
        String bestSense = simplifiedLesk(stem, stems);
        System.out.print("- " + stem + " // ");
        if (bestSense != null) {
          System.out.println("best sense: " + bestSense);
        }
        else {
          System.out.println("no need disambiguation");
        }
      }
    }
  }


}
