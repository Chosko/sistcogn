public class Progressbar {
  
  private float loop;
  private float loopLength;
  private float barLength;
  private float updateEvery;
  private float updateAt;
  private float step;
  private float indentation;

  public Progressbar(int loopLength){
    indentation = 0;
    init(loopLength);
  }

  public Progressbar(int loopLength, int indentation){
    indentation = indentation;
    init(loopLength);
  }

  public void init(int loopLength){
    this.loop = 0;
    this.loopLength = loopLength;
    this.barLength = 50;
    this.updateEvery = (float)loopLength/barLength;
    this.updateAt = this.updateEvery;
    this.step = 0;

    printIndentation();
    System.out.print("|");
    for (int i=0; i<barLength; i++) {
      System.out.print("-");
    }
    System.out.print("|\n");
    printIndentation();
    System.out.print("|");
  }

  private void printIndentation(){
    for (int i=0;i<indentation;i++) {
      System.out.print(" ");
    }
  }

  public void step(){
    loop++;
    while(updateAt < loop){
      step++;
      System.out.print(".");
      updateAt += updateEvery;
    }
    if (loop >= loopLength) {
      if (step < barLength) {
        System.out.print(".");
      }
      System.out.print("|\n");
    }
  }
}