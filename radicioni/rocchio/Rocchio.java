import java.nio.file.Path;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.io.*;
import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.jlt.util.Language;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.POS;

import javax.print.Doc;

public class Rocchio {

  public final static String BABELNET_CONFIG = "./lib/config/babelnet.properties";
  public final static String LEMMARIO = "./lib/lemmario.txt";
  public final static String STOPWORDS = "./lib/stopwords.txt";
  public final static String DOC_FOLDER = "./lib/docs";

  // how many training set documents for each class
  public final static int TRAININGSET = 19;
  // # of documents for each class
  public final static int CLASSCARDINALITY = 20;
  // Weight for Positive Class Docs
  public final static float BETA = 16;
  // Weight for Negative Class Docs
  public final static float GAMMA = 4;

  // the set of documents D
  public static DocumentLibrary library = new DocumentLibrary();

  // Read all documents from file
  public static void readAllDocuments() throws IOException{
    Files.walk(Paths.get(DOC_FOLDER)).forEach(filePath -> {
      if (Files.isRegularFile(filePath)) {
        try{
          String content = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
          Path p = filePath.getFileName();
          final Document doc = new Document(filePath.getFileName().toString(), content);
          library.add(doc);
        }
        catch(IOException ex){
          throw new RuntimeException("File not found");
        }
      }
    });
  }

  public static void main(String[] args) throws IOException{


    // System.out.println("Loading BabelNet...");
    // BabelNetConfiguration bc = BabelNetConfiguration.getInstance();
    // bc.setConfigurationFile(new File(BABELNET_CONFIG));
    // BabelNet bn = BabelNet.getInstance();
    // System.out.println();

    // Read all documents
    System.out.print("Reading all documents... ");
    readAllDocuments();
    System.out.println("ok - " + library.size() + " documents read");

    // Read stop words from file
    System.out.print("Reading stop words from file... ");
    HashSet<String> stopWords = NLUtil.readStopWords(STOPWORDS);
    System.out.println("ok - " + stopWords.size() + " stop words read");

    // Read lemmario from file
    System.out.print("Reading the lemmario from file... ");
    Map<String,String[]> lemmario = NLUtil.readLemmario(LEMMARIO);
    System.out.println("ok - " + lemmario.size() + " lemmas read");

    // Clean the documents (or use BabelSense ids???)
    System.out.println("Cleaning the documents...");
    Progressbar pb = new Progressbar(library.size());
    for (Document doc: library.documents) {
      doc.cleanContent(stopWords);
      pb.step();
    }

    // Lemmatise the documents (or use BabelSense ids???)
    System.out.println("Lemmatising the documents...");
    pb.init(library.size());
    for (Document doc: library.documents) {
      doc.setLemmas(lemmario);
      pb.step();
    }

    // Create the dictionary
    System.out.print("Creating the global dictionary... ");
    int nTerm = library.createDictionary();
    System.out.println("ok - " + nTerm + " terms included");

    // Extract the features... from now a document is identified by its feature
    System.out.println("Calculating Feature Vector for each document");
    pb.init(library.size());
    for (Document doc: library.documents) {
      // di fatto prende il feature vector di parole prima costruito
      // e poi per ogni documento calcola la frequenza del termine all'interno del documento
      // e la normalizza (forse,vediamo)
      doc.calculateFeatureVector(library.dictionary);
      pb.step();
    }

    // Calculate rocchio
    System.out.println("Building Class Profiles (Rocchio)...");
    library.calculateRocchio();

    // Foreach class
    System.out.println("Calculating distance between query and profiles...");
    int docs = 0;
    LinkedHashMap<String,Integer> matches = new LinkedHashMap<String,Integer>();
    matches.put("global",0);
    for (Document doc: library.documents) {
      if (!doc.isTrainingSet()) {
        String recClass = library.query(doc);
        System.out.println("Doc:\t"+ doc.filename);
        System.out.println("Category:\t"+ doc.getCategory());
        System.out.println("Rocchio:\t"+ recClass);

        int n = 0;
        try {
          n = matches.get(doc.getCategory());
        }
        catch (NullPointerException e) {}


        if (doc.getCategory().equals(recClass)) {
          matches.put(doc.getCategory(), n+1);
          matches.put("global", matches.get("global")+1);
        } else {
          matches.put(doc.getCategory(), n);
        }

        System.out.println("--------------------");
        docs++;
      }
    }

    String tabs;
    for (Map.Entry<String, Integer> match : matches.entrySet()) {
       tabs = "\t";
      if (match.getKey().length() <= 5) {
        tabs = "\t\t";
      }
      if(!match.getKey().equals("global"))
        System.out.println("["+ match.getKey() +"]"+ tabs + (float)match.getValue()/(CLASSCARDINALITY-TRAININGSET+1)*100 +"%\t["+ match.getValue() +"/"+ (CLASSCARDINALITY-TRAININGSET+1) +"]");
      else
        System.out.println("["+ match.getKey() +"]"+ tabs + (float)match.getValue()/docs*100 +"%\t["+ match.getValue() +"/"+ (docs) +"]");
    }


    System.out.println("Finished!");
  }

  /**
   * rappresenta un vettore di feature, praticamente
   * serve alla library per rappresentare le query su tutta
   * la libreria
   *
   *
   * boh qua mi sembra di fare una copai del progetto
   * di dbm solo con meno tempo.
   *
   */
  public static class FeatureVector {
    public HashMap<String,Float> featureVector;

    public FeatureVector() {
      featureVector = new HashMap<String,Float>();
    }

    private FeatureVector(HashMap<String,Float> featureVector){
      this.featureVector = featureVector;
    }

    public FeatureVector clone(){
      HashMap<String,Float> fvClone = (HashMap<String,Float>)featureVector.clone();
      return new FeatureVector(fvClone);
    }

    public Set<String> getFeatures(){
      return featureVector.keySet();
    }

    public Collection getValues(){
      return featureVector.values();
    }

    public float getWeight(String lemma) {
      return featureVector.get(lemma);
    }

    private boolean increment(String lemma) {
      if(exists(lemma)){
        featureVector.put(lemma, featureVector.get(lemma) + 1);
        return true;
      }
      else{
        return false;
      }
    }

    // cerca un lemma all'interno del feature vector
    public boolean exists(String lemma) {
      return featureVector.containsKey(lemma);
    }

    public void add(String lemma) {
      if(exists(lemma)){
        increment(lemma);
      }
      else{
        featureVector.put(lemma, 1.0f);
      }
    }

    public boolean addWithoutIncrement(String lemma, float weight){
      if (exists(lemma)) {
        return false;
      }
      else{
        featureVector.put(lemma, weight);
        return true;
      }
    }

    public int size() {
      return featureVector.size();
    }

/*    public float distance(FeatureVector other) {
      float[] vector = new float[10];
      float distance = 0, dot = 0, modthis = 0, modOther = 0;

      for (int i=0;i<vector.length; i++) {
        dot += this.getWeight(i)*other.getWeight(i);
        modthis += Math.pow(this.getWeight(i),2);
        modOther += Math.pow(other.getWeight(i),2);
      }

      return dot/(Math.sqrt(modthis)*Math.sqrt(modOther));
    }*/

    public double distance(FeatureVector other) {
      double distance = 0, dot = 0, modthis = 0, modOther = 0;

      for (String l: this.getFeatures()) {
        dot += this.getWeight(l)*other.getWeight(l);
        modthis += Math.pow(this.getWeight(l),2);
        modOther += Math.pow(other.getWeight(l),2);
      }

      return dot/(Math.sqrt(modthis)*Math.sqrt(modOther));
    }

  }


  // Document Class, represent a single document along with
  // its category, filename and content
  public static class Document {

    //nome del file
    private String filename;
    // contenuto grezzo del documento
    private String content;
    // contenuto pulito (dalle stop words, punteggiatura e a capi)
    private String[] cleanedContent;
    // categoria del documento prelevata dal filename
    private String category;
    // id del documento, in realtà inutile dato che ci sono numeri ripetuti per ogni categoria
    private int id;
    // boolean per settare il training set in modo da fare la roba del riconoscimento
    private Boolean trainingset;
    // array di lemmi (matrice) dove il primo valore è la stringa stessa mentre il secondo ??? il pos?
    private String[][] lemmas;
    // categoria riconosciuta dall'algoritmo
    private String recognized_category;
    // vettore delle feature di questo document, viene salvata la frequenza
    FeatureVector fv;

    public Document(String fn, String c) {
      filename = fn;
      content = c;
      category = extractCategory(fn);
      id = extractID(fn);
      trainingset = isTrainingSet();
      lemmas = null;

      // initialize the feature vector class, una classe
      fv = new FeatureVector();

      recognized_category = "UNKNOWN";
    }

    /**
     * return document category
     * @return
     */
    public String getCategory(){
      return category;
    }

    public float getFeatureWeight(String lemma) {
      return fv.getWeight(lemma);
    }

    /**
     * Estrae la categoria dal filename
     */
    private String extractCategory(String filename) {
      return filename.split("_")[0];
    }

    // estrae l'id del document, a partire dal nome
    // cicico_01, => 01
    private int extractID(String filename) {
      String[] s = filename.split("_");
      String p = s[s.length-1].split("\\.")[0];
      return Integer.parseInt(p);
    }

    public boolean isTrainingSet(){
      return (id < TRAININGSET);
    }

    // imposta il contenuto grezzo del documento
    public void setContent(String c){
      content = c;
    }

    public void cleanContent(HashSet<String> stopWords){
      cleanedContent = NLUtil.cleanSentence(content, stopWords);
    }

    public String[][] getLemmas(){
      return lemmas;
    }

    public void setLemmas(Map<String,String[]> lemmario){
      lemmas = NLUtil.lemmatise(cleanedContent, lemmario);
    }

    public void calculateFrequency(){
      for (String[] lemma: lemmas){
        // il lemma[0] esiste già nel bag of words
        if (fv.exists(lemma[0])) {
          fv.increment(lemma[0]);
        }
      }
    }

    public String toString(){
      return "=========================" +
             "\ndoc: "+ category +"_"+ id +
             "\nrecognized: "+ recognized_category;
    }

    public void calculateFeatureVector(FeatureVector dictionary){
      fv = dictionary.clone();
      for (String lemma[] : lemmas) {
        fv.add(lemma[0]);
      }
    }

    public boolean isOfClass(String docClass) {
      return category.equals(docClass);
    }

  }

  // stub per classe lemma che probabilmente ci servirà
  public static class Lemma {
    //bubblenet id?
    private int id;
    // stringa del lemma
    private String name;
    // gloss
    private String gloss;

    public Lemma(int id, String name, String gloss) {
      this.id = id;
      this.name = name;
      this.gloss = gloss;
    }
  }

  /**
   *
   */
  public static class DocumentLibrary {

    // list of documents
    // note: each document has a dictionary
    public ArrayList<Document> documents;
    // dictionary T = {t1,...tm}
    public FeatureVector dictionary = new FeatureVector();
    // dobbiamo creare un vettore che chiameremo C_i <f_1i, ..., f_ni>,
    // dove C_i rappresenta una classe di documento e f_ki rappresenta
    // la k-th feature del documento i.
    // CALCOLO
    // dunque per calcolare C_i potremmo usare FeatureVector
    // rocchioList = list.size == ClassList.size
    // foreach Class in ClassList
    //    foreach Feature in Dictionary
    //      ClassFeatureVector.add(calculateRocchio(documentList))
    //    end
    // end
    //
    // per calcolare rocchio dobbiamo sommare i pesi della feature "f"
    // all'interno dei documenti della classe "c" (moltiplicato per beta e diviso per il numero di
    // documenti nella classe) e sottrarre la somma dei pesi della feature "f" nei documenti
    // non appartenenti alla classe "c"
    //
    // calculateRocchio(DocumentList docs, Class c, Feature f) -> float rocchio;
    //    float rocchio = 0;
    //
    //    float pos = 0;
    //    float neg = 0;
    //    foreach doc in documentList
    //        if doc.isTrainingSet
    //          weight = doc.getWeight(f)
    //          if doc.isClass(c)
    //            pos += weight
    //          else
    //            neg += wight
    //          end
    //        end
    //    end
    //
    //    r = beta*pos/c.size() - gamma*neg/(docs.size() - c.getCardinalita.size())

    // C_i sarà all'interno di una variabile ClassFeatureVector, che sarà
    // una lista di FeatureVector
    public ArrayList<FeatureVector> profiles = new ArrayList<FeatureVector>();
    public ArrayList<String> profileLabels = new ArrayList<String>();
    public FeatureVector classes = new FeatureVector();

    public DocumentLibrary(){
      documents = new ArrayList<Document>();
    }

    // get the number of documents inside the library
    public int size() {
      return documents.size();
    }

    // add a document inside a library
    public void add(Document doc){
      documents.add(doc);
      classes.add(doc.getCategory());
    }


    public Document get(int index) {
      return documents.get(index);
    }

    // Crea il dizionario globale, e ritorna il numero di termini totale.
    public int createDictionary(){
      int n = 0;
      for (Document doc : documents) {
        String[][] lemmas = doc.getLemmas();
        for (String[] lemma : lemmas) {
          if(dictionary.addWithoutIncrement(lemma[0],0))
            n++;
        }
      }
      return n;
    }

    public void calculateRocchio() {
      Progressbar pb = new Progressbar(classes.size());
      // TODO: lemma: conversion to class Feature
      for (String docClass: classes.getFeatures()) {
        // feature vector represent a class
        FeatureVector vectorClass = new FeatureVector();
        for (String lemma : dictionary.getFeatures()) { // for each feature
          vectorClass.addWithoutIncrement(lemma, calculateFeatureWeight(lemma, docClass));
        }
        profiles.add(vectorClass);
        profileLabels.add(docClass);
        pb.step();
      }
    }

    public float calculateFeatureWeight(String lemma, String docClass) {
      float fWeight = 0, pos = 0, neg = 0;

      // per ogni document andiamo a prelevare la frequenza del lemma
      for (Document doc: documents) {
        // escludiamo i documenti non nel training set
        if (doc.isTrainingSet()) {
          float weight = doc.getFeatureWeight(lemma);
          if (doc.isOfClass(docClass)) {
            pos += weight;
          }
          else {
            neg += weight;
          }
        }
      }
      fWeight = BETA*pos/CLASSCARDINALITY - GAMMA*neg/(documents.size() - CLASSCARDINALITY);
      return fWeight;
    }

    public String query(Document q){
      double minDistance = Double.MIN_VALUE, distance = 0;
      String minDistanceClass = "";
      int i = 0;
      for (String docLabel: profileLabels) {
        distance = q.fv.distance(profiles.get(i));
        if (distance > minDistance) {
          minDistance = distance;
          minDistanceClass = docLabel;
        }
        i++;
      }
      return minDistanceClass;
    }
  }
}
