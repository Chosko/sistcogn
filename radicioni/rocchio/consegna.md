# Classificazione con ausilio delle risorse semantiche: il metodo di Rocchio

## terminologia

* Documento: unità di testo indicizzata nel sistema e disponibile per la ricerca.
* Collezione: insieme di documenti, utilizzati per soddisfare le richieste dell'utente
* Termine: elemento (dal singolo elemento lessicale a intere phrases) che occorre nella collezione
* Interrotgazione: la richiesta dell'utente, espressa come insieme di termini

## vector space model

Documento espresso da feature, che sono vettori i cui elementi sono il numero di ricorrenze di un dato termine

Anche le query espresse come vettori

Utilizziamo la distanza coseno (vedi slides 14 per formula con tf idf). Si può calcolare anche come dot product normalizzato

## Pesatura dei termini

usiamo il criterio tf idf

w_(i,j) = tf_(i,j) * idf_(i,j)

i è il termine i-esimo, j il documento j_esimo

## Metodo di Rocchio

### Profilo di una classe di documenti

Alcuni classificatori lineari consistono di un *profilo esplicito* (o documento prototipo) della categoria

Il learning di un classificatore lineare è spesso preceduto da un *local term space reduction*. 
* In questo caso, un profilo c_i è una lista pesata dei termini la cui presenza o assenza è più importante per discriminare c_i

### Il centroide

Sia **D** l'insieme dei documenti e **T** il dizionario (l'insieme di tutti i differenti termini che occorrono in D)

T = {t_1,...,t_m}

Allora la frequenza assoluta del termine **t** nel documento **d** è data da **tf(d,t)**

Denotiamo il vettore dei termini:

t_d = (tf(d,t_1), ... , tf(d,t_m))

Il **centroide** di un insieme di X vettori di termini è definito come

t_X = (1/|X|) * Somma di tutti i vettori t_d

### Metodo di Rocchio

c_i = (f_(1i), ..., f_(|T|i))

* Per ogni classe c_i (con i che varia sulle classi) calcoliamo il peso della k-esima feature f come

j_ki = beta * somma(w_kj/|POS_i|) - gamma * somma(w_kj/NEG_i)

(vedi slide 19)

beta e gamma sono parametri di controllo che permettono di impostare l'importanza relativa degli esempi positivi e negativi

* Se beta è impostato a 1 e gamma a 0, il profilo di c è il centroide dei suoi esempi positivi

Un classificatore costruito con Rocchio premia:

* la vicinanza di un documento di test al centroide degli esempi positivi
* la distanza dal centroide degli esempi negativi

### Raffinamento: i near positives

* Usare invece di NEG_i NPOS_i, cioè premiare non la distanza da tutti gli esempi negativi, ma dagli esempi negativi più vicini ai positivi
* I near positives sono i documenti negativi più difficili da distinguere dai positivi

# Consegna

* scrivere un programma che utilizzi una rappresentazione basata sul feature vector model
    - Quali feature scegliere? I lemmi dei termini in input o i relativi BabelNet IDs? nel secondo caso è necessario predisporre qualche forma di WSD
    - Implementando entrambe le alternative, quali differenze nei risultati ci possiamo attendere?
* il programma deve costruire i profili (Rocchio) relativi alle 10 classi in cui si articolano i documenti
    - partire con beta=16 e gamma=4
    - provare a utilizzare l'algoritmo con NPOS
* deve classificare i nuovi documenti (utilizzando come metrica di distanza la cosine similarity o una delle metriche derivate) in una delle 10 classi date

