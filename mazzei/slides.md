# Glossario Sistemi Cognitivi (senza tanti catsi)

## Lemmi

* **Fonema**/**Phoneme**: Un fonema è un suono di una lingua (fono) che ha valore distintivo, ossia può produrre variazioni di significato se scambiato con un altro fonema.
ad esempio, la differenza di significato tra l'italiano tetto e detto è il risultato dello scambio tra il fonema /t/ e il fonema /d/.
* **Morfema**/**Morpheme**: In linguistics, a morpheme is the smallest grammatical unit in a language.
A morpheme is not identical to a word, and the principal difference between the two is that a morpheme may or may not stand alone, whereas a word, by definition, is freestanding.
"Unbreakable" comprises three morphemes: un- (a bound morpheme signifying "not"), -break- (the root, a free morpheme), and -able (a free morpheme signifying "can be done").
* **Parola**/**Word**: Minima unità isolabile dotata di significato e di una funzione autonomi, formata da uno o più fonemi.
* **Fono/Phone**: Un fono è un suono linguistico, ossia un suono del linguaggio umano, prodotto dall'apparato fonatorio.

## Discipline Linguistiche (le capacità di HAL)

* Fonetica e Fonologia: the study of linguistics sounds
* Morphology: the study of meaningful components of words
* Syntax: the study of structural relationships between words
* Semantics: the study of meaning
* Pragmatics: the study of how language is used to accomplish goals
* Discourse: the study of linguistic unit larger than a single utterance (affermazione)

## Struttre linguistiche

Una struttura è un insieme su cui è definita una relazione

* relazione **fonetico-fonologica** sull’insieme dei **foni-fonemi**
* relazione **morfologica** sull’insieme dei **morfemi**
* relazione **sintattica** sull’insieme delle **parole**
* relazione **semantica** sull’insieme dei **significati** delle parole
* relazione **pragmatica** sull’insieme dei **significati** dell parole e sul **contesto**
* rlazione “**discorsale**” sull’insieme delle **frasi**

## Ambiguità

I made her duck

1. Le parole *duck* e *her* sono **morfologicamente ambiguo** nelle loro POS (part of speech): *duck* può essere verbo o sostantivo, mentre *her* può esere pronome dativo o possessio
2. Il verbo *make* è **sintatticamente ambiguo** in un modo diverso. Può essere transitivo o ditransitivo
3. Il verbo *make* è **semanticamente ambiguo**. Significa creare o cucinare

## Problema Fondamentale

**Convertire una frase o un testo in una forma che permetta l'applicazione di meccanismi di ragionamento (automatico) **

### Moduli per raggiungere questo risultato

* Il livello morfologico e l’analisi del lessicale (lessico: il complesso delle parole e delle locuzioni di una lingua o di una parte di essa)
  * parole di contenuto e di funzione
  * analizzatore morfologico e PoS tagging
* Il livello lessicale
  * sintassi e grammatiche
  * costituenti e le CFG (??? cosa sono)
  * le grammatiche a dipendenze
* il livello semantico
  * semantica lessicale
  * semantica guidata dalla sintassi

## Livello morfologico e analisi lessicale

**Lessico** basato sul concetto di **parola**: sequenza di caratteri delimitata da spazi o punteggiatura

Scompone parole in sequenze di più parole (passa-me-la)

Mette insieme sequenze che hanno un significato unitario (di corsa/by the way)

### Part of speech (PoS)

determiner, noun, verb, adjective, adverb, preposition, conjunction, pronoun, interjection

## Livello sintattico

**Syntactic Parsing**: derivare una struttura sintattica dalla sequenza di parole

### Tipi di struttura sintattica

* **Struttura a costituenti**: rappresenta le “relazioni di raggruppamento” tra le parole
* **Struttura a dipendenze**: rappresenta le “relazioni di dipendenza” tra le parole

### Costituenza

Come sappiamo cos’è un costituente?

* Distribuzione: un costituente si comporta come un’unità che può apparire in diversi posti:
  * John talked [to the children] [about drugs]
  * John talked [about drugs] [to the children]
* Sostituzione/espansione/pro-forms
  * I sat [on the box/right on top of the box/there]

**Costituente** = gruppo di parole contigue

* che si comportano come un’unità
* che hanno delle proprietà sintattiche
* in CFG (grammatiche context free) i costituenti sono i **simboli non terminali**

### Dipendenza

Relazione tra due parole:

* Head: parola dominante
* Dependent: parola dominata

La head seleziona i suoi dipendenti e determina le loro proprietà (es: il verbo determina il numero dei suoi argomenti)

## Livello semantico

### Semantica lessicale

**Semantica lessicale**: basata sui **lessemi**

Lessema = coppia *forma-significato* = elemento del lessico

* *forma* ortografica e fonologica
* *significato* (senso)

#### Relazioni tra lessemi

* **Omonimia** (homonymy): 2 lessemi con la stessa forma (ortografica) e due sensi diversi
* **Polisema** (polysemy): lo stesso lessema (stessa forma) ha due sensi diveri (es: pesca)
* **Zeugma**: il collegamento di un verbo a due o più elementi della frase che invece richiederebbero ognuno rispettivamente un verbo specifico (es: Giovanni ha aperto il gioco e la finestra)
* **Sinonimia** (synonymy): due lessemi con forma diversa hanno lo stesso senso
* **Iponimia** (hyponymy): due lessemi di cui uno denota una sottoclasse dell’altro (es: automobile è un iponimo di veicolo)
* **Iperonimia** (hyperonymy): il contrario di iponimia

### Semantica composizionale

**Semantica composizionale**: traduzione dal linguaggio naturale in una qualche forma di rappresentazione della conoscenza (ispirata alla semantica dei linguaggi formali. Es: significato di “X + Y” in aritmetica)

La semantica di un **sintagma** è funzione della semantica dei sintagmi componenti; non dipende da altri sintagmi esterni al sintagma stesso. Conoscendo il significato di X, Y e +, possiamo comporre il significato “X+Y”

Esempio: “Giorgio ama Maria” -> Ama(Giorgio, Maria).

Si usa il **lambda calcolo**

## Pragmatica

es. “oggi mi trovo ad Alessandria” -> interpretazione dipende dal contesto, ossia “io” e “oggi”

### Anafora: sintagmi che si riferiscono ad oggetti precedentemente citati, es. la torta era sul tavolo. Paolo **la** divorò

## Anatomia di un parser

Un parser è compsto da:

* **Grammatica**: Context-Free, TAG, CCG, Dependency….
* **Algoritmo**
  * Search strategy:
    * top-down: solo ricerche che possono portare a risposte corrette, cioè con radice S. Comporta la creazione di alberi non compatibili con le parole 
    * bottom-up: solo ricerche compatibili con le parole. Comporta la creazione di alberi non corretti
  * Memory organization: back-tracking,dynamic programming…
* **Oracolo**: Probabilistico, rule-based…

## Dynamic programming

* Sottostrutture ottimali (????)
* Sottoproblemi sovrapponibili (???)
* Memoizzazione: una tecnica di programmazione che consiste nel salvare in memoria i valori restituiti da una funzione in modo da averli a disposizione per un riutilizzo successivo senza doverli ricalcolare
* Simile a dividi et impera

### CKY algorithm (Cocke-Kasami-Younger)

Calcola tutti i possibili parse in tempo O(n^3). Il difetto è che il caso peggiore e il caso medio coincidono (ma anche esige una forma normale per la grammatica)

* Grammatica: Context-Free
* Algoritmo:
  * Search strategy: bottom-up
  * Memory organization: dynamic programming
* Oracolo: rule-based

Restrizione: la grammatica deve essre Context Free espressa in **forma normale di Chomsky**:

* Copia tutte le regole conformi senza cambiarle
* Converti i terminali all’interno delle regole in non-terminali dummy
* Converti le unit-productions
* Binarizza tutte le regole e aggiungile alla grammatica
* Epsilon free

* [demo](http://www.diotavelli.net/people/void/demos/cky.html)
* [wiki](http://en.wikipedia.org/wiki/CYK_algorithm)
* [boh](http://people.cs.nctu.edu.tw/~wgtzeng/courses/FL2013SpringUnder/CYK%20algorithm.pdf)


Pseudo Code
```
let the input be a string S consisting of n characters: a1 ... an.
let the grammar contain r nonterminal symbols R1 ... Rr.
This grammar contains the subset Rs which is the set of start symbols.
let P[n,n,r] be an array of booleans. Initialize all elements of P to false.
for each i = 1 to n
  for each unit production Rj -> ai
    set P[i,1,j] = true
for each i = 2 to n -- Length of span
  for each j = 1 to n-i+1 -- Start of span
    for each k = 1 to i-1 -- Partition of span
      for each production RA -> RB RC
        if P[j,k,B] and P[j+k,i-k,C] then set P[j,i,A] = true
if any of P[1,n,x] is true (x is iterated over the set s, where s are all the indices for Rs) then
  S is member of language
else
  S is not member of language
```

#### CKY probabilistico

lol

### Chunck parsing

lol



## Parser a dipendenze

* sintassi a dipendenze 
* approcci per il parsing a dipendenze
* parsing deterministico a “transizioni”
* parsing a regole per dipendenze a vincoli

### Criteri sintattici per distinguere una head (H) e un dependent (D) in una costruzione (C)

* H determina la categoria sintattica di C; H può sostituire C
* H è obbligatoria; D può essere opzionale
* H seleziona D e determina quando D è obbligatoria
* La forma di D dipende da H (agreement)
* La posizione nella frase di D è specificabile con riferimento a H
* H determina la categoria *semantica* di C

### Tecniche per il dependency parsing

* Dynamic programming: simile a CKY. complessità O(n^3)
* Graph algorithms: si crea un Maximum Spanning Tree per la frase
* Costraint satisfaction: Vengono eliminate tutte le possibili dipendenze che non soddisfano certi vincoli
* Deterministic parsing: Scelte greedy per la creazione di dipendenze tra parole, guidate da machine learning classifiers
* Costituency parsing + conversion: Parsing con una grammatica a costituenti. Converto in un formato a dipendenze attraverso delle "tabelle di percolazioni" (teoria X-bar).

#### Parsing deterministico a "transizioni"

* Grammatica: Dependency grammar (TB)
* Algoritmo:
  * Search strategy: bottom-up
  * Memory organization: depth-first
* Oracolo: Probabilistico

stato composto da tre componenti: 
* stack che contiene la lista delle parole parzialmente analizzate
* una lista che contiene le parole da analizzare
* un insieme che contiene le dipendenze binarie tra parole

stato iniziale
[ [root], [sentence], [] ]
stato finale
[ [root], [], R]
dove R è l’insieme delle dipendenze costruite

esempio:
s.i.: [[root], [I booked a morning fight], ()]
s.f.: [[root], [], ((booked, I) (booked, fight) (fight, a) (fight, morning))] 

#### Parsing a regole per dipendenze a vincoli (TUP - Turin University Parsing)

* Grammar: Dependency grammar (constraints)
* Algrorithm:
  * Search strategy: bottom-up
  * Memory organization: depth-first
* Oracolo: rule-based

**Regole**: Chunking, Coordination, Verb-SubCat

**Dipendenze**: Morpho-syntactic, Syntactic-functional, Semantic

## Semantica computazionale (Computational semantics)

* **Frase**: Paolo corre. Tutti quelli che corrono vivono meglio.
* **Analisi sintattica**: (S (NP Paolo) (VP corre))
* **Analisi semantica**: run(paolo)
* **Inferenza**:
  * $ \forall x.run(x) \rightarrow goodLife(x) $
  * $ run(paolo) $

### FoL syntax (First order Logic)

* **Simboli**
  - Costanti: john, mary
  - Predicati e relazioni: man, walks, loves
  - Variabili: x,y
  - Connettivi Logici: $ \land \lor \neg \rightarrow $
  - Quantificatori: $ \forall \exists $
  - Simboli ausiliari: (),
* **Formule**
  - Formule atomiche: loves(paolo, francesca)
  - Formule composte: man(paolo) $ \land $ loves(paolo, francesca)
  - Formule quantificate: $ \exists $x (man(x))

### Algoritmo fondamentale

* Parsificare la frase per ottenere l'albero sintattico (costituenti)
* Cercare la semantica di ogni parola nel lessico
* Costruire la semantica per ogni sintagma
  * Bottom-up
  * Syntax-driven: "rule-to-rule translation"

Il significato della frase è costruito:

* dal significato delle parole: **lessico**
* risalendo le costruzioni sintattiche: **regole semantiche**

### Introduzione al Lambda Calcolo

#### Glossario Minimale
* **λ**, simbolo metalogico che segna informazione mancante
* **Function Application**, $(λx.love(x, mary))(john)$
* **Beta Reduction**, attività elementare che permette di eliminare il λ
* **Grammatica semantica**: comprende Lessico e Regole di Composizione
  - Lessico
    + NP: $paolo \rightarrow Paolo $
    + NP: $francesca \rightarrow Francesca $
    + V: $λy.λx.love(x,y) \rightarrow ama $
  - Regole di Composizione
    + VP: $ f(a) \rightarrow V:f, NP:a
    + S: $f(a) \rightarrow NP:a, VP:f

#### Slides

* serve per risolvere il problema di legare le variabili libere
es. love(?,francesca) -> non FoL, love(x,francesca) -> variabile libera?
es2. love(?, francesca) o love(francesca, ?)

$ λx.love(x, francesca) \rightarrow amare Francesca $

il nuovo simbolo metalogico $λ$ segna l'informazione mancante nella metaformula λ-FoL; permette di astrarre su $x$

Come combinare λ-FoL e FoL: Function application

* $(λx.love(x, mary)) @ john$
* $(λx.love(x, mary))(john)$

La FA è un'operazione elementare chiamata *Beta Reduction*

$(λx.love(x, mary)) (john)$

eliminando il λ,

$(love(x,mary))(john)$

rimuovendo l'argomento

$love(x,mary)$

rimpiazza le occorrenze della variabile legata dal λ con l'argomento di tuttal a formula

$love(john,mary)$

es. Paolo ama Francesca
1.
Paolo - NP: paolo
ama - V: λy.λx.love(x,y)
Francesca - NP: francesca
2. VP: λy,λx.love(x,y)(francesca) = λx.love(x,francesca)
3. S: λx.love(x,francesca)(paolo) = love(paolo,francesca)

Bisogna poi mappare gli elementi di significato (le formule FoL) al mondo
* FoL termini -> elementi del dominio
  - Med -> "f"
* FoL formule atomiche -> insiemi sui domini di elementi o insiemi di tuple
* noisy(Med) è vera se "f" è nell'insieme degli elmenti della relazione noisy
* near(Med, Rio) è vera se la tupla `<f,g>` è nell'insieme delle tuple che corrispondono a "near" nell'interpretazione 


### Semantica Computazionale per:

#### Articoli indeterminativi

**Un**: DT: $ \lambda P.\lambda Q.\exists z (P(z) \land Q(z)) $

**tutti**: DT: $ \lambda P.\lambda Q.\forall z (P(z) \rightarrow Q(z)) $

**nessun**: DT: $ \lambda P.\lambda Q.\forall z (P(z) \rightarrow \neg Q(z)) $

#### Nomi comuni

**Uomo**: N: $ \lambda x.man(x) $

#### Nomi propri

**NON HO CAPITO** la spiegazione... c'entra qualcosa il type-raising

**Paolo**: NP: $ \lambda P.P(Paolo) $

#### Verbi transitivi

**NON HO CAPITO** la spiegazione... c'entra qualcosa il type-raising

**Ama**: V: $ \lambda R.\lambda x.R(\lambda y.love(x,y)) $

#### Verbi intransitivi

**Corre**: V: $ \lambda x.run(x) $

#### Coordinazione dei nomi

**Paolo e Francesca corrono**: $ run(Paolo) \land run(Francesca) $

Per esprimere la correlazione si aggiunge al lessico:

**e**: CC: $ \lambda X.\lambda Y.\lambda R.(X(R)\land Y(R)) $

#### Scope ambiguity (quantificatori)

Every man loves a woman

1. $ \forall x(man(x) \rightarrow \exists y (woman(y) \land love(x,y))) $
2. $ \exists y (woman(y) \land \forall x(man(x) \rightarrow love(x,y))) $

## Natual Language Generation

### Glossario
* **NLG** è il processo di costruire deliberatamente un testo in linguaggio natuale in modo da soddisfare certi obiettivi di comunicazione.
* **NLP** NLU (natual language understanding) + NLG (natural language generation), ma molto più il primo perché tendenzialmente per produrre un testo sufficientemente informativo è sufficiente un insieme prefissato di frasi. NLG più semplice e quindi meno interessante

### Cos'è
* *Scopo generale* sist automatico per produrre testo
* *input* una forma di rappresentazione dell'informazione di qls origine
* *output* documenti, etc
* *kb, knowledge bases* sul linguaggio, sul dominio



### I task della NLG

1. Content determination
2. Discourse planning
3. Sentence aggregation
4. Lexicalisation
5. Referring expression generation
6. Syntactic and morphological realization
7. Orthographic realization

#### Content determination

Cosa dire?

* **Messaggi** provenienti da una struttura dati
* I messaggi sono aggregazioni di dati che possono essere espressi linguisticamente, con una parola o un sintagma
* I messaggi sono basati su entità, concetti e relazioni sul dominio

#### Discourse planning

Un testo non è solo una collezione casuale di frasi. C'è una struttura che le relaziona.

2 Tipi di relazioni:

* conceptual grouping
* rhetorical relationships

#### Sentence aggregation

Un mapping 1-a-1 dai messaggi alle frasi produrrebbe un testo poco naturale, non fluente.

I messaggi devono essere combinati per produrre frasi più complesse ma anche più naturali.

Il risultato è un **sentence specification o sentence plan**

* Senza aggregazione
  - The next train is the Caledonian Express
  - It leaves Aberdeen at 10 am.
* Con aggregazione
  - The next train, which leaves at 10 am, is the Caledonian Express

#### Lexicalisation

#### Referring expression generation
* generare le "referring expressions", determina quali parole usare per esprimere le entità del dominio in una maniera comprensibile all'utente
* bilanciare fluenza e ambiguità
  - Nomi Propri (Aberdeen, Scotland $\rightarrow$ Aberdeen)
  - Descrizione definite (the train that leaves at 10am $\rightarrow$ the next train)

#### Syntactic and morphological realization

Ogni lingua possiede, ed ha regole specifiche per produrre

* morfologia: come si formano le parole: es. walk+ed per formare passato
* sintassi: come si formano le frasi es. soggetto prima del verbo (John Walked)

#### Ortographic realization

* maiuscole, punteggiatura, formattazione

* frasi iniziano con maiuscola
* frasi dichiarative finiscono con punto

### Architettura standard

* Text Plannin -> Sentence Planning -> Linguistic Realizazion

* Text Planning
  - content determination
  - discourse planning
* Sentence Planning
  - sentence aggregation
  - lexicalisation
  - referring expression generation
* Linguistic Realization
  - syntax + morphology
  - ortografica realization


# Da vedere
* CKY probabilistico
* Chunck parsing
