# Sentence Planner

require 'yaml'

print "reading FoL formulas... "
f = File.open("../data/folsem.txt", "r")
sentences = []
f.each_line { |line| sentences << line.chop }
f.close
puts "ok"
plans = []

sentences.each do |sentence|
  print "making sentence plan of: #{sentence}... "
  plan = {"clauses" => []}
  det_regex = /exists\s*(\w*).\(\s*(\w*)\([\w\s]+\)(\s*&\s*\w*\([\w\s]+(,[\w\s]+)*\))*\s*\)/
  all_regex = /all\s*(\w+).\(\s*\w+\(([\w\s]+)\)\s*->\s*\w+\([\w\s]+(,[\w\s]+)*\)(\s*&\s+\w+\([\w\s]+(,[\w\s]+)*\))*\s*\)/
  replaced = true
  while replaced
    replaced = false
    det_scanned = sentence.scan det_regex
    all_scanned = sentence.scan all_regex
    if det_scanned.any?
      det_scanned.each_with_index do |el,i|
        old_term = el[0]
        new_term = el[1]
        s = sentence.match(det_regex)[0]
        s.gsub! /exists\s*#{old_term}.\(/, ""
        s.gsub! /#{new_term}\(#{old_term}\)\s*\&\s*/, ""
        s.chop!
        s.gsub!(/(\W)#{old_term}(\W)/){|match| match[0] << "a " << new_term << match[1+old_term.length]} 
        sentence.sub! det_regex, s
      end
      replaced = true
    end
    if all_scanned.any?
      all_scanned.each_with_index do |el,i|
        old_term = el[0]
        new_term = el[1]
        s = sentence.match(all_regex)[0]
        s.gsub! /all\s*#{old_term}.\(/, ""
        s.gsub! /\s*#{old_term}\(#{new_term}\)\s*->\s*/, ""
        s.chop!
        s.gsub!(/(\W)#{old_term}(\W)/){|match| match[0] << "all " << new_term.sub(/a\s*/,"") << match[1+old_term.length]}
        sentence.sub! all_regex, s
      end
      replaced = true
    end
  end
  clauses = sentence.scan(/(\w+)\((\w[\w\s]*)(\,(\w[\w\s]*)(\,(\w[\w\s]*))?)?\)/)
  clauses.each do |clause|
    c = {}
    c["verb"] = clause[0] if
    phrases = {
      "subj" => 1,
      "obj" => 3,
      "pp" => 5
    }
    phrases.each_pair do |name, val|
      if clause[val]
        pos = clause[val].scan(/(\w+)(\s+(\w+))?/)
        c[name] = {}
        if pos[0][2]
          c[name]["det"] = pos[0][0]
          c[name]["noun"] = pos[0][2]
          case pos[0][0]
          when "a"
            c[name]["num"] = "singular"
          when "all"
            c[name]["num"] = "plural"
          end
        else
          c[name]["noun"] = pos[0][0]
          c[name]["num"] = "singular"
        end
        c[name]["pre"] = "to" if name == "pp"
      end
    end
    plan["clauses"] << c
  end
  puts "ok"
  plans << plan
end

yaml = plans.to_yaml
f = File.open("../data/sentenceplan.yml", "w")
f.print yaml
f.close