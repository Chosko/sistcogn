# Consegna progetto

1. Scrivere una CFG per l'italiano (G1) che parsifichi (almeno) le frasi di esempio (sentences-01)
2. Implementare in Java l'algoritmo CKY e provare la grammatica G1 su questo programma per le frasi in sentences-01
3. Dotare di semantica la grammatica G1 (G2) e provarla in NLTK
4. Costruire un sentence planner che per ogni formula prodotta dalla grammatica G2 produca un sentence plan (abstract-syntax tree a dipendenze) valido. HINT: è possibile usare un semplice approccio basato su espressioni regolari e template
5. Usando la libreria SimpleNLG (eventualmente come server attraverso socket o via pipe) implementare un semplice realizer che trasformi i sentence plans in frasi inglesi.
6. BONUS: estrarre una PCFG dal Penn-TUT e provarla in NLTK con le frasi sentences-01 

## Sentences-01

    Paolo ama Francesca
    un uomo ama Francesca
    Francesca odia un uomo
    Paolo e Dante sognano Francesca e Beatrice
    tutti gli uomini adorano una donna
    un uomo adora tutte le donne
    Caronte naviga
    Dante e un uomo visitano un inferno
    una donna e Dante lasciano un dono a Virgilio
    Dante rivede le stelle

