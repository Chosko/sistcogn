# Scrivere una CFG per l'italiano (G1) che parsifichi (almeno) le frasi di esempio (sentences-01, vedi dopo)

## Glossario

[Riferimento POS e costituenti](http://aclweb.org/anthology//J/J93/J93-2004.pdf)

### costituenti (nodi non terminali)

* S: Start
* NP: Noun phrase
* PP: Prepositional phrase
* VP: Verb phrase
* ADJP: Adjective phrase
* ADVP: Adverb phrase

### PoS (nodi terminali)

* CC: Coordinating conjunction (e)
* DT: Determiner
* IN: Preposition/subordinating conjunction
* JJ: Adjective
* NN: Noun, singular or mass
* NNS: Noun, plural
* NNP: Proper noun, singular
* NNPS: Proper noun, plural
* RB: Adverb
* TO: to
* VB: Verb, base form
* VBD: Verb, past tense
* VBG: Verb, gerund/present participle
* VBN: Verb, past participle
* VBP: Verb, non-3rd ps. sing. present
* VBZ: Verb, 3rd ps. sing. present
* PDT: Predeterminer (tutti, nessuno ecc...)

## Grammatiche che generano le singole frasi

Scriviamo una per una le grammatiche che generano le singole frasi

Per testarle possiamo usare lo [Stanford Parser](http://nlp.stanford.edu:8080/parser/) per vedere se genera alberi simili (non è necessario che siano identici), ovviamente traducendo le frasi in inglese

Alcune grammatiche sono difficili. Conviene farle prima semplici (anche se non in forma normale) e poi normalizzarle in CNF usando [questo algoritmo](http://en.wikipedia.org/wiki/Chomsky_normal_form#Converting_a_grammar_to_Chomsky_Normal_Form)

Per testare le grammatiche con CKY: [http://www.diotavelli.net/people/void/demos/cky.html](http://www.diotavelli.net/people/void/demos/cky.html)

### Paolo ama Francesca

    S -> NP VP
    NP -> NNP
    VP -> VBZ NP
    VBZ -> ama
    NNP -> Paolo
    NNP -> Francesca

CNF

    S -> NP VP
    VP -> VBZ NP
    VBZ -> ama
    NP -> Paolo
    NP -> Francesca

### Un uomo ama Francesca

    S -> NP VP
    NP -> NNP
    NP -> DT NN
    VP -> VBP NP
    VBP -> ama
    NNP -> Francesca
    NN -> uomo
    DT -> un

CNF

    S -> NP VP
    NP -> DT NN
    VP -> VBP NP
    VBP -> ama
    NP -> Francesca
    NN -> uomo
    DT -> un

### Francesca odia un uomo

    S -> NP VP
    NP -> NNP
    NP -> DT NN
    VP -> VBZ NP
    VBZ -> odia
    NNP -> Francesca
    DT -> un
    NN -> uomo

CNF

    S -> NP VP
    NP -> DT NN
    VP -> VBZ NP
    VBZ -> odia
    NP -> Francesca
    DT -> un
    NN -> uomo

### Paolo e Dante sognano Francesca e Beatrice

    S -> NP VP
    NP  -> NP CC NP
    VP  -> VBP NP
    VBP -> sognano
    NNP -> Paolo
    CC  -> e
    NNP -> Dante
    NNP -> Francesca
    NNP -> Beatrice

CNF

    S -> NP VP
    NP  -> NP CCNP
    CCNP -> CC NP
    VP  -> VBP NP
    VBP -> sognano
    NP -> Paolo
    CC  -> e
    NP -> Dante
    NP -> Francesca
    NP -> Beatrice

### Tutti gli uomini adorano una donna

    S - NP VP
    NP - PDT DT NNS
    NP -> DT NN
    VP -> VBP NP
    VBP -> adorano
    PDT -> tutti
    DT -> gli
    DT -> una
    NNS -> uomini
    NN -> donna

CNF

    S -> NP VP
    NP -> PDT DTNNS
    DTNNS -> DT NNS
    NP -> DT NN
    VP -> VBP NP
    VBP -> adorano
    PDT -> tutti
    DT -> gli
    DT -> una
    NNS -> uomini
    NN -> donna

### Un uomo adora tutte le donne

    S -> NP VP
    NP  -> PDT DT NNS
    NP  -> DT NN
    VP  -> VBZ NP
    VBZ -> adora
    PDT   -> tutte
    DT  -> le
    DT  -> un
    NN  -> uomo
    NNS -> donne

CNF

    S -> NP VP
    NP  -> PDT DTNNS
    DTNNS -> DT NNS
    NP  -> DT NN
    VP  -> VBZ NP
    VBZ -> adora
    PDT   -> tutte
    DT  -> le
    DT  -> un
    NN  -> uomo
    NNS -> donne

### Caronte naviga

    S -> NP VP
    NP -> NNP
    VP -> VBZ
    VBZ -> naviga
    NNP -> Caronte

CNF

    S -> NP VP
    VP -> naviga
    NP -> Caronte

### Dante e un uomo visitano un inferno

    S -> NP VP
    NP  -> NP CC NP
    NP  -> DT NN
    NP  -> NNP
    VP  -> VBP NP
    NNP -> dante
    CC  -> ee
    VBP -> visitano
    DT  -> un
    NN  -> inferno
    NN  -> uomo

### Una donna e Dante lasciano un dono a Virgilio

    S -> NP VP
    NP -> NP CC NP
    NP -> DET NN
    NP -> NNP
    VP -> VBP NP PP
    PP -> TO NP
    DET -> una
    DET -> un
    NN -> donna
    NN -> dono
    CC -> e
    NNP -> Dante
    NNP -> Virgilio
    VBP -> lasciano
    TO -> a

CNF

    S -> NP VP
    NP -> NP CCNP
    CCNP -> CC NP
    NP -> DET NN
    VP -> VBP NPPP
    NPPP -> NP PP
    PP -> TO NP
    DET -> una
    DET -> un
    NN -> donna
    NN -> dono
    CC -> e
    NP -> Dante
    NP -> Virgilio
    VBP -> lasciano
    TO -> a

### Dante rivede le stelle

    S -> NP VP
    NP  -> DT NNS
    NP  -> NNP
    VP  -> VBP NP
    VBP -> rivede
    NNP -> Dante
    DT  -> le
    NNS -> stelle

CNF

    S -> NP VP
    NP  -> DT NNS
    VP  -> VBP NP
    VBP -> rivede
    NP -> Dante
    DT  -> le
    NNS -> stelle


## Grammatica finale

### Paolo ama Francesca

    S -> NP VP
    NP -> DT NN
    NP -> DT NN PP
    NP -> DT NNS
    NP -> NNP
    NP -> NP CC NP
    NP -> PDT DT NNS
    VP -> VB
    VP -> VB NP
    PP -> TO NP
    VB -> adora
    VB -> adorano
    VB -> ama
    VB -> lasciano
    VB -> naviga
    VB -> odia
    VB -> rivede
    VB -> sognano
    VB -> visitano
    NNP -> Beatrice
    NNP -> Caronte
    NNP -> Dante
    NNP -> Francesca
    NNP -> Paolo
    NNP -> Virgilio
    NNS -> donne
    NNS -> stelle
    NNS -> uomini
    NN -> donna
    NN -> dono
    NN -> inferno
    NN -> uomo
    PDT -> tutte
    PDT -> tutti
    DT -> gli
    DT -> le
    DT -> un
    DT -> una
    CC -> e
    TO -> a

CNF

    S -> NP VP
    NP -> DT NN
    NP -> DT NNPP
    NNPP -> NN PP
    NP -> DT NNS
    NP -> NP CCNP
    CCNP -> CC NP
    NP -> PDT DTNNS
    DTNNS -> DT NNS
    VP -> VP NP
    PP -> TO NP
    VP -> adora
    VP -> adorano
    VP -> ama
    VP -> lasciano
    VP -> naviga
    VP -> odia
    VP -> rivede
    VP -> sognano
    VP -> visitano
    NP -> Beatrice
    NP -> Caronte
    NP -> Dante
    NP -> Francesca
    NP -> Paolo
    NP -> Virgilio
    NNS -> donne
    NNS -> stelle
    NNS -> uomini
    NN -> donna
    NN -> dono
    NN -> inferno
    NN -> uomo
    PDT -> tutte
    PDT -> tutti
    DT -> gli
    DT -> le
    DT -> un
    DT -> una
    CC -> e
    TO -> a
