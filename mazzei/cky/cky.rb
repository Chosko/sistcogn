# CKY algorithm

# Read the grammar from file
rules = [] # the rules of the grammar
puts "Reading grammar rules from file..."
f = File.open("../data/g1.txt", "r")
f.each_line do |line|
  rule = {
    lhs: line.split(">")[0], # the left side of the rule
    rhs: line.split(">")[1].chop.split # the right side of the rule (chop removes \n at the end of string)
  }
  rules << rule
end
f.close

# Read the sentences from file
sentences = [] # the sentences to test
puts "Reading sentences from file..."
f = File.open("../data/sentences-01.txt")
f.each_line do |line|
  sentences << line.chop
end
f.close

# Look into the rules if there is a rule that produce the terminal token 
def look_terminal(rules, token)
  results = []
  rules.each do |rule|
    if rule[:rhs].length == 1 && rule[:rhs][0] == token
      results << rule[:lhs]
    end
  end
  return results # return empty array if no rule found
end

# Look into the rules if there is a rule 'rule -> first second'
def look_rhs(rules, first, second)
  results = []
  rules.each do |rule|
    first.each do |f|
      second.each do |s|
        if rule[:rhs].length == 2 && rule[:rhs][0] == f && rule[:rhs][1] == s
          results << rule[:lhs]
        end
      end
    end
  end
  return results
end

# print on stdout and log file
def myprint file, str
  print str
  file.print str
end

def myputs file, str
  puts str
  file.puts str
end

f = File.open("../data/cky.log", "w")
f.puts "Report of the execution of cky.rb"

# For each sentence, run CKY
sentences.each do |s|
  myprint f, "Testing CKY for: " << s << "... "

  
  # Initialization
  tokens = s.split  # tokenize the sentence
  width = tokens.length  # the width of the CKY table
  table = Array.new(width) { |i| Array.new(width-i){[]} } # create the table!
  
  # Fill the first row
  tokens.each_with_index do |t,i|
    table[0][i] = look_terminal(rules,t)
  end

  for i in 1...width
    for j in 0...width-i
      for p in 0...i
        first = table[i-1-p][j]
        second = table[p][j+i-p]
        table[i][j].concat(look_rhs(rules,first,second)).uniq
      end
    end
  end

  if table[width-1][0].include?("S")
    myputs f, "ok"
  else
    myputs f, "error"
    myprint f, table
    myputs f, ""
  end
end

f.close