La grammatica in inglese dava troppi problemi...

Provo a generare una grammatica in italiano da 0

## Brainstorming

Provo a usare un approccio bottom-up

### PoS

Cerco di indivituare i part of speech a partire dalle frasi di esempio

    Paolo ama Francesca
    Un uomo ama Francesca
    Francesca odia un uomo
    Paolo e Dante sognano Francesca e Beatrice
    Tutti gli uomini adorano una donna
    Un uomo adora tutte le donne
    Caronte naviga
    Dante e un uomo visitano un inferno
    Una donna e Dante lasciano un dono a Virgilio
    Dante rivede le stelle

#### Articoli

##### Articolo determinativo maschile plurale

    ADMP -> gli

##### Articolo determinativo femminile plurale

    ADFP -> le

##### Articolo indeterminativo maschile

    AIM -> un

##### Articolo indeterminativo femminile

    AIF -> una

#### Quantificatori

##### Quantificatore maschile plurale

    QMP -> tutti

##### Quantificatore femminile plurale

    QFP -> tutte

#### Nomi

Nomi propri e nomi comuni. Possono essere maschili, femminili, singolari e plurali

##### Nome proprio

    NNP -> Dante | Virgilio | Caronte | Beatrice | Francesca

##### Nome comune maschile singolare

    NCMS -> uomo | inferno | dono

##### Nome comune maschile plurale

    NCMP -> uomini

##### Nome comune femminile singolare

    NCFS -> donna

##### Nome comune femminile plurale

    NCFP -> donne

#### Verbi

Usiamo solo verbi al presente e alla terza persona (singolare o plurale). Secondo me la cosa più semplice è distinguerli così

##### Verbo transitivo singolare

    VTS -> ama | odia | adora | rivede

##### Verbo transitivo plurale

    VTP -> sognano | adorano | visitano

##### Verbo intransitivo singolare

    VIS -> naviga

##### Verbo transitivo plurale con complemento di termine

    VTPT -> lasciano

#### Congiunzioni

    CC -> e

#### Preposizioni

    PR -> a

### Costituenti (sintagmi o phrase)

#### SNP (singular noun phrase)

Bisogna suddividere la classica NP in singolare e plurale perchè a seconda del numero va abbinata a un diverso verbo

può essere con nomi propri e comuni. Per i nomi comuni bisogna gestire gli articoli e i quantificatori

    SNP -> NNP (Paolo, Francesca, Beatrice, Dante, Virgilio)
        | AIM NCMS (un uomo, un dono, un inferno)
        | AIF NCFS (una donna)

#### PNP (plural noun phrase)

    PNP -> SNP CC SNP (Paolo e Dante, Francesca e Beatrice, una donna e Dante)
        | QMP ADMP NCMP (tutti gli uomini)
        | QFP ADFP NCFP (tutte le donne)

Così sta roba gestisce correlazioni senza ricorsione

#### SVP (singular verb phrase)

    SVP -> VTS SNP (ama Francesca, odia un uomo)
        | VTS PNP (adora tutte le donne, rivede le stelle)
        | VIS (naviga)

#### PVP (plural verb phrase)

    SVP -> VTP SNP (adorano una donna, visitano un inferno)
        | VTP PNP (sognano Francesca e Beatrice)
        | VTPT SNP PR SNP (lasciano un dono a Virgilio)

#### S (start)

    S -> SNP SVP
        | PNP PVP

## Glossario

### Costituenti

### PoS

## Grammatica