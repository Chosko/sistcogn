# Esercitazione di Sistemi Cognitivi

Parte del Prof. Mazzei.

Implementata da Ruben Caliandro e Roberto Pesando.


***Table of Contents***

[TOC]

## Consegna

1. Scrivere una CFG per l'italiano (G1) che parsifichi (almeno) le frasi di esempio (sentences-01)
2. Implementare in Java l'algoritmo CKY e provare la grammatica G1 su questo programma per le frasi in sentences-01
3. Dotare di semantica la grammatica G1 (G2) e provarla in NLTK
4. Costruire un sentence planner che per ogni formula prodotta dalla grammatica G2 produca un sentence plan (abstract-syntax tree a dipendenze) valido. HINT: &egrave; possibile usare un semplice approccio basato su espressioni regolari e template
5. Usando la libreria SimpleNLG (eventualmente come server attraverso socket o via pipe) implementare un semplice realizer che trasformi i sentence plans in frasi inglesi.

## Implementazione

L'esercitazione &egrave; stata svolta in diversi linguaggi: Ruby, Python, Java e YAML.

Le varie fasi dell'esercitazione sono state sviluppate separatamente, in una struttura a pipeline. Per eseguire l'intera pipeline basta utilizzare lo script `runall.sh` nella root del progetto.

Gli script, oltre a scrivere su stdout, scrivono dei files di output (nella cartella `data`) che servono sia come input per altri script che come file di log da conslutare separatamente.

### Struttura delle directory

* **cky**: contiene lo script dell'algoritmo CKY (parte 2)
* **semantic**: contiene lo script che utilizza NLTK per generare le formule logiche (parte 3)
* **planner**: contiene lo script che genera il sentence plan (parte 4)
* **realiser**: contiene il programma java che genera le frasi in inglese (parte 5)
* **data**: contiene i file di input/output delle varie fasi:
    - **sentences-01.txt**: file creato a mano che contiene le frasi richieste dalla consegna
    - **g1.txt**: file creato a mano che contiene la grammatica (parte 1).
    - **cky.log**: file di output dell'algoritmo CKY (parte 2). Contiene l'esito dell'algoritmo per le varie frasi.
    - **g2.fcfg**: file creato a mano che contiene la grammatica arricchita con la semantica (parte 3)
    - **folsem.txt**: file di output dello script che usa NLTK (parte 3). Contiene le formule logiche che rappresentano la semantica delle frasi.
    - **sentenceplan.yml**: file di output dello script di sentence planning (parte 4). Contiene il sentence plan.
    - **sentence-02.txt**: file di output del realiser (parte 5). Contiene le frasi tradotte in inglese
* **clean.sh**: script bash che pulisce i binari e i files di output delle varie fasi (ma non i file creati a mano)
* **project.md**: la consegna dell'esercitazione
* **relazione.md** e **relazione.html**: questa relazione
* **runall.sh**: script bash che pulisce, compila ed esegue tutta la pipeline

Di seguito viene spiegata l'implementazione delle varie parti dell'esercitazione.

### 1. Scrivere una CFG per l'italiano

Partendo dall'elenco delle frasi in italiano presente nel file `sentence-01.txt` e dallo _start symbol_ `S -> NP VP` abbiamo costruito, frase per frase, una CFG che riuscisse parsarla.

es. *sentence*:

* Paolo ama Francesca

CFG:

```ruby
# start symbol
S -> NP VP
# non terminale dummy per produrre i nomi propri
NP -> NNP
# verbo transitivo
VP -> VBZ NP
# terminali
VBZ -> ama
NNP -> Paolo
NNP -> Francesca
```

Altre regole introdotte nelle seguenti frasi sono

```ruby
# es. un uomo
NP -> DT NN

```

Ricordiamo che CKY richiede una grammatica espressa in CNF (Chomsky Normal Form), per cui alcune regole sono state riscritte seguendo le seguenti regole:

* Convertire i terminali all’interno delle regole in non-terminali dummy
* Binarizza tutte le regole e aggiungile alla grammatica

Ad esempio la regola

```ruby
# (Dante) (e) (Virgilio)
NP -> NP CC NP
```

E' stata trasformata in

```ruby
NP -> NP CCNP
CCNP -> CC NP
```

### 2. Algoritmo CKY (Ruby)

L'algoritmo CKY &egrave; stato scritto in ruby. Per prima cosa legge le regole della grammatica da file, inserendole in un array di regole `rules`.

Ogni regola `rule` all'interno dell'array `rules` &egrave; un hash, formato da due campi: `:lhs` che contiene la parte sinistra della produzione e `:rhs` che contiene la parte destra della produzione

```ruby
# Read the grammar from file
rules = [] # the rules of the grammar
puts "Reading grammar rules from file..."
f = File.open("../data/g1.txt", "r")
f.each_line do |line|
  rule = {
    lhs: line.split(">")[0], # the left side of the rule
    rhs: line.split(">")[1].chop.split # the right side of the rule (chop removes \n at the end of string)
  }
  rules << rule
end
f.close
```

Anche le frasi vengono lette da file e inserite nell'array `sentences`

Poi, per ogni `sentence` in `sentences`, viene eseguito l'algoritmo che segue.

La stringa `sentence` viene suddivisa in `tokens` e viene creata la tabella triangolare per CKY, usando un array tridimensionale (la terza dimensione serve per i bucket, in cui verranno inseriti gli eventuali match)

```ruby
 # Initialization
  tokens = s.split  # tokenize the sentence
  width = tokens.length  # the width of the CKY table
  table = Array.new(width) { |i| Array.new(width-i){[]} } # create the table!
```

La tabella &egrave; *triangolare* nel senso che data una frase di n caratteri (per esempio "Dante rivede le stelle", quindi n=4), la struttura risulta essere di questo tipo:

```ruby
# sentence
"Dante rivede le stelle"

# table, con width = 4
[
    [[],[],[],[]],  # Riga 0: n elementi
    [[],[],[]],     # Riga 1: n-1 elementi
    [[],[]],
    [[]]            # Riga n: 1 elementi
]

# NB: questo è solo uno schema per scopi dimostrativi, non fa parte del codice
```

Il risultato &egrave; una tabella simile a quella vista a lezione, ma ruotata. Le celle della tabella sono a loro volta array (che fungono da bucket), perch&egrave; potrebbe esserci pi&ugrave; di un match

La prima riga viene subito riempita con la parte sinistra delle produzioni che producono i terminali della `sentence`.

```ruby
# Fill the first row
tokens.each_with_index do |t,i|
    table[0][i] = look_terminal(rules,t)
end
```

`look_terminal(rules,t)` &egrave; una funzione che cerca il terminale `t` nella grammatica e ritorna tutte le parti sinistre delle regole che producono `t`, raggruppate in un array.

```ruby
# Look into the rules if there is a rule that produce the terminal token 
def look_terminal(rules, token)
  results = []
  rules.each do |rule|
    if rule[:rhs].length == 1 && rule[:rhs][0] == token
      results << rule[:lhs]
    end
  end
  return results # return empty array if no rule found
end
```

Nello schema seguente viene mostrata la tabella, dopo il riempimento della prima riga

```ruby
# sentence
"Dante rivede le stelle"

# table, con width = 4
[
    [["NP"],["VP"],["DT"],["NP"]],  # Riga 0: n elementi
    [[],[],[]],     # Riga 1: n-1 elementi
    [[],[]],
    [[]]            # Riga n: 1 elementi
]

# NB: questo è solo uno schema per scopi dimostrativi, non fa parte del codice
```

Una volta riempita la prima riga, vengono eseguiti i cicli annidati dell'algoritmo CKY.

```ruby
  for i in 1...width
    for j in 0...width-i
      for p in 0...i
        first = table[i-1-p][j]
        second = table[p][j+i-p]
        table[i][j].concat(look_rhs(rules,first,second)).uniq
      end
    end
  end
```

`look_rhs` &egrave; una funzione analoga a `look_terminal`, ma che cerca nelle parti destre delle regole i non terminali `first` e `second`, e ritorna tutte le parti sinistre delle regole che li producono, raggruppate in un array.

```ruby
# Look into the rules if there is a rule 'rule -> first second'
def look_rhs(rules, first, second)
  results = []
  rules.each do |rule|
    first.each do |f|
      second.each do |s|
        if rule[:rhs].length == 2 && rule[:rhs][0] == f && rule[:rhs][1] == s
          results << rule[:lhs]
        end
      end
    end
  end
  return results
end
```

La correttezza viene testata controllando se nell'ultima riga (che ha solo un bucket) compare il simbolo "S".

Il risultato viene salvato nel file `data/cky.log`

### 3. CFG+Semantica e NLTK (Python)

La grammatica scritta e testata con l'algoritmo CKY &egrave; stata dotata di semantica; grazie al lambda calcolo &egrave; possibile creare delle formule in _first order logic_ senza ambiguit&agrave; durante la sostituzione delle variabili.

Per il parsing &egrave; stata usata la libreria NLTK in Python, che permette di inserire _features_ all'interno delle regole in CFG.

Le lambda sono state inserite sui terminali in modo che l'algoritmo, risalendo, sostituisca parametri e funzioni in base a quanto specificato nelle formule.

Le formule pi&ugrave; semplici sono quelle dei verbi intransitivi, come _naviga_, che diventa

```ruby
# intransitive verb
IV[NUM=sg,SEM=<\x.(sail(x))>] -> 'naviga'
```

dove la feature `NUM` indica la "cardinalit&agrave;" del verbo, singolare, mentre `SEM` esplicit&agrave; la function application

$ \lambda x.sail(x) $

NOTA: la sintassi del file fcfg codifica le $\lambda$ come `\` e tra parentesi angolari le function applications `<f>`; In questo caso il parametro della function application &egrave; `x`. Se non si inseriscono le parentesi angolari `<f>` la stringa &egrave; passata _as-is_.

la _lhs_ della formula di sopra viene matchata dalla _rhs_ seguente

```ruby
VP[NUM=?n,SEM=?v] -> IV[NUM=?n,SEM=?v]
```

dove le feature `NUM` e `SEM` vengono trasferite al non-terminale `VP`

Il predicato in FoL viene scritto nella regola il cui _lhs_ &egrave; lo _start symbol_ S:

```ruby
# S -> NP VP
S[SEM=<?subj(?vp)>] -> NP[NUM=?n,SEM=?subj] VP[NUM=?n,SEM=?vp]
```

Dove una function application senza lambda costruisce il predicato prendendo le due parti della semantica (come variabili) `?subj`, il soggetto, e `?vp`, il verbo e ci&ograve; che ne consegue.

Le altre _part of speech_ sono state tradotte dalle slides e alcune costruite ad-hoc.

in particolare

```ruby
# articoli
DT[NUM=sg,SEM=<\P \Q.exists x.(P(x) & Q(x))>] -> 'un'
# pronomi
PDT[SEM=<\P \Q.all x.(P(x) -> Q(x))>] -> 'tutti'
# sostantivi
NN[NUM=sg,SEM=<\x.man(x)>] -> 'uomo'
NN[NUM=pl,SEM=<\x.man(x)>] -> 'uomini'
# nomi propri
PropN[SG=Male,NUM=sg,SEM=<\P.P(Paolo)>] -> 'Paolo'
# verbi transitivi
V[NUM=sg,SEM=<\X x.X(\y.love(x,y))>] -> 'ama'
# verbi transitivi "ternari"
DTV[NUM=pl,SEM=<\Y X x.X(\z.Y(\y.give(x,y,z)))>] -> 'lasciano'
# congiunzioni
CC[SEM=<\X \Y R.(X(R) & Y(R))>] -> 'e'
# preposizioni
P[+TO] -> 'a'
```

NOTA: viene effettuato anche il matching della "cardinalit&agrave;", per cui `gli uomini` viene parsato correttamente, mentre "un uomini", no.


### 4. Sentence Planner (Ruby)

Il sentence planner prende in input le formule logiche generate nella fase precedente e produce un sentence plan molto semplice in formato YAML.

Per prima cosa inserisce le formule in un array `sentences`

```ruby
f = File.open("../data/folsem.txt", "r")
sentences = []
f.each_line { |line| sentences << line.chop }
f.close
```

poi, per ogni formula `sentence` esegue il codice descritto sotto.

Viene creato un hash `plan`, che conterr&agrave; il sentence plan di `sentence`.

```ruby
plan = {"clauses" => []}
```

`plan` contiene al suo interno un array `clauses` (proposizioni). 

* Viene utilizzato un array perch&egrave; ci sono dei casi (come "Dante e un uomo visitano un inferno") in cui la formula logica &egrave; stata scomposta in pi&ugrave; frasi correlate ("Dante visita un inferno AND un uomo visita un inferno").
* Per tutti gli altri casi, `clauses` sar&agrave; un array con un solo elemento, cio&egrave; il plan della frase stessa.

Vengono poi definite delle espressioni regolari che serviranno per individuare gli articoli determinativi e i quantificatori nella formula:

```ruby
det_regex = /exists\s*(\w*).\(\s*(\w*)\([\w\s]+\)(\s*&\s*\w*\([\w\s]+(,[\w\s]+)*\))*\s*\)/
all_regex = /all\s*(\w+).\(\s*\w+\(([\w\s]+)\)\s*->\s*\w+\([\w\s]+(,[\w\s]+)*\)(\s*&\s+\w+\([\w\s]+(,[\w\s]+)*\))*\s*\)/
```

Queste espressioni regolari individuano solo le espressioni "semplici", cio&egrave; non sono in grado di trovare espressioni annidate. Per questo motivo viene eseguito un ciclo `while` che ad ogni passo "riduce" la formula in una forma pi&ugrave; semplice, permettendo &ograve; al passo successivo di ridurre ulteriormente la formula, fino a portarla a una forma base.

Il seguente schema di esempio illustra pi&ugrave; nel dettaglio cosa avviene ad ogni ciclo

```ruby
# formula originale
"exists x.(man(x) & all z5.(exists z4.(woman(z4) & z5(z4)) -> worship(x,z5)))"

# Il primo passo del ciclo trova "exists z4.(woman(z4) & z5(z4))" e lo riduce:
"exists x.(man(x) & all z5.(z5(a woman) -> worship(x,z5)))"

# Il secondo passo trova "all z5.(z5(a woman) -> worship(x,z5))" e lo riduce:
"exists x.(man(x) & worship(x,all woman))"

# Il terzo passo trova "exists x.(man(x) & worship(x,all woman))" e lo riduce:
"worship(a man,all woman)"

# NB: questo è solo uno schema per scopi dimostrativi, non fa parte del codice
```

Una volta che la formula &egrave; stata ridotta ad una forma base (come `worship(a man,all woman)`), le `clauses` vengono prelevate con una semplice espressione regolare.

```ruby
clauses = sentence.scan(/(\w+)\((\w[\w\s]*)(\,(\w[\w\s]*)(\,(\w[\w\s]*))?)?\)/)
```

Questa espressione &egrave; in grado di riconoscere sia i verbi intransitivi (che hanno un solo argomento) che i verbi transitivi (che possono avere due o tre argomenti nel caso di "x dona qualcosa a y")

A seconda dei casi, vengono estratte le varie componenti (sempre con un'espressione regolare), che possono essere `verb`, `subj`, `obj` o `pp`, a seconda della loro posizione nella formula ridotta.

A loro volta `subj`, `obj` e `pp` vengono scomposti in:

* `det` (opzionale, &egrave; l'articolo)
* `noun` (obbligatorio, &egrave; il nome)
* `num` (obbligatorio, pu&ograve; assumere i valori "singular" o "plural")
* `pre` (opzionale, &egrave; l'eventuale preposizione "to")

Al termine dell'esecuzione, si &egrave; venuto a formare un albero per ogni formula. Di seguito ne viene riportato un esempio, per la formula `all x.(exists z2.(man(z2) & x(z2)) -> exists z3.(woman(z3) & worship(x,z3)))`:

```yaml
- clauses:
  - verb: worship
    subj:
      det: all
      noun: man
      num: plural
    obj:
      det: a
      noun: woman
      num: singular
```

Il sentence plan risultante &egrave; un file YAML (`data/sentenceplan.yml`) che contiene gli alberi costruiti per ogni frase.

### 5. Realiser (SimpleNLG/Java)

Il realiser legge il sentence plan e produce le frasi in un inglese corretto, utilizzando la libreria java SimpleNLG.

Il sentence plan creato nella frase precedente &egrave; una lista di alberi serializzata in YAML, dove gli alberi rappresentano le singole frasi.

Quello che fa il realiser &egrave; "visitare" ogni albero in input, per ricreare un albero analogo in SimpleNLG. Dato che la struttura del sentence plan &egrave; stata pensata apposta per questo scopo, il programma esegue quasi una riscrittura 1 a 1 dei nodi prelevati dal file YAML.

Le frasi prodotte vengono salvate nel file `data/sentences-02.txt`

## Risultati

Riportiamo i risultati delle varie parti dell'esercitazione:

Le frasi di partenza sono:

    Paolo ama Francesca
    un uomo ama Francesca
    Francesca odia un uomo
    Paolo e Dante sognano Francesca e Beatrice
    tutti gli uomini adorano una donna
    un uomo adora tutte le donne
    Caronte naviga
    Dante e un uomo visitano un inferno
    una donna e Dante lasciano un dono a Virgilio
    Dante rivede le stelle
    Dante e Virgilio vendono Beatrice e Francesca a un uomo

### 1. Scrivere una CFG per l'italiano

CFG in forma normale di Chomsky

    S>NP VP
    NP>DT NN
    NP>NP PP
    NP>DT NNS
    NP>NP CCNP
    CCNP>CC NP
    NP>PDT DTNNS
    DTNNS>DT NNS
    VP>VP NP
    PP>TO NP
    VP>adora
    VP>adorano
    VP>ama
    VP>lasciano
    VP>naviga
    VP>odia
    VP>rivede
    VP>sognano
    VP>visitano
    VP>vendono
    NP>Beatrice
    NP>Caronte
    NP>Dante
    NP>Francesca
    NP>Paolo
    NP>Virgilio
    NNS>donne
    NNS>stelle
    NNS>uomini
    NN>donna
    NN>dono
    NN>inferno
    NN>uomo
    PDT>tutte
    PDT>tutti
    DT>gli
    DT>le
    DT>un
    DT>una
    CC>e
    TO>a

### 2. Algoritmo CKY (Ruby)

Esito dell'algoritmo CKY sulle frasi 

    Report of the execution of cky.rb
    Testing CKY for: Paolo ama Francesca... ok
    Testing CKY for: un uomo ama Francesca... ok
    Testing CKY for: Francesca odia un uomo... ok
    Testing CKY for: Paolo e Dante sognano Francesca e Beatrice... ok
    Testing CKY for: tutti gli uomini adorano una donna... ok
    Testing CKY for: un uomo adora tutte le donne... ok
    Testing CKY for: Caronte naviga... ok
    Testing CKY for: Dante e un uomo visitano un inferno... ok
    Testing CKY for: una donna e Dante lasciano un dono a Virgilio... ok
    Testing CKY for: Dante rivede le stelle... ok
    Testing CKY for: Dante e Virgilio vendono Beatrice e Francesca a un uomo... ok

### 3. CFG+Semantica e NLTK (Python)

Grammatica arricchita con la semantica:

```ruby
% start S
############################
# Grammar Rules
#############################
# S -> NP VP
S[SEM=<?subj(?vp)>] -> NP[NUM=?n,SEM=?subj] VP[NUM=?n,SEM=?vp]

# NP -> DT NN
# es. un uomo
# num must agree
NP[NUM=?n,SEM=<?dt(?nn)>] -> DT[NUM=?n,SEM=?dt] NN[NUM=?n,SEM=?nn]
# es. Dante
NP[NUM=?n,SEM=?np] -> PropN[NUM=?n,SEM=?np]

# NP -> NP PP
#NP[SEM=?np] -> NP[SEM=?np] PP[SEM=?pp]

# VP -> VP NP
VP[SEM=<?v(?obj)>] -> V[SEM=?v] NP[SEM= ?obj]
VP[NUM=?n,SEM=?v] -> IV[NUM=?n,SEM=?v]
VP[NUM=?n,SEM=<?v(?obj,?pp)>] -> DTV[NUM=?n,SEM=?v] NP[SEM=?obj] PP[+TO,SEM=?pp]

# NP -> NP CCNP
# CCNP -> CC NP
NP[SEM=<?cc(?np1,?np2)>] -> NP[SEM=?np1] CC[SEM=?cc] NP[SEM=?np2]

# NP -> PDT DTNNS
NP[SEM=<?pdt(?np)>] -> PDT[SEM=?pdt] NP[SEM=?np]

# PP -> TO NP
PP[+TO, SEM=?np] -> P[+TO] NP[SEM=?np]
#############################
# Lexical Rules
#############################

# DT -> un
DT[NUM=sg,SEM=<\P \Q.exists x.(P(x) & Q(x))>] -> 'un'
# DT -> una
DT[NUM=sg,SEM=<\P \Q.exists x.(P(x) & Q(x))>] -> 'una'
# DT -> gli
# DT -> le
DT[NUM=pl,SEM=<\P \Q.exists x.(P(x) & Q(x))>] -> 'gli'
DT[NUM=pl,SEM=<\P \Q.exists x.(P(x) & Q(x))>] -> 'le'
# λ P.λ Q.λ z.(P(z) -> Q(z))
# PDT -> tutte
# PDT -> tutti
PDT[SEM=<\P \Q.all x.(P(x) -> Q(x))>] -> 'tutti'
PDT[SEM=<\P \Q.all x.(P(x) -> Q(x)) >] -> 'tutte'

# singular
NN[NUM=sg,SEM=<\x.man(x)>] -> 'uomo'
NN[NUM=sg,SEM=<\x.woman(x)>] -> 'donna'
NN[NUM=sg,SEM=<\x.gift(x)>] -> 'dono'
NN[NUM=sg,SEM=<\x.hell(x)>] -> 'inferno'
# plural
# TODO: verificare il plurale, soprattutto con TUTTE, forse bisogna
# creare un non terminale per gestire il plurale --> forall
NN[NUM=pl,SEM=<\x.man(x)>] -> 'uomini'
NN[NUM=pl,SEM=<\x.woman(x)>] -> 'donne'
NN[NUM=pl,SEM=<\x.star(x)>] -> 'stelle'

# PROPN
PropN[SG=Male,NUM=sg,SEM=<\P.P(Paolo)>] -> 'Paolo'
PropN[SG=Male,NUM=sg,SEM=<\P.P(Dante)>] -> 'Dante'
PropN[SG=Male,NUM=sg,SEM=<\P.P(Caronte)>] -> 'Caronte'
PropN[SG=Male,NUM=sg,SEM=<\P.P(Virgilio)>] -> 'Virgilio'
PropN[SG=Female,NUM=sg,SEM=<\P.P(Francesca)>] -> 'Francesca'
PropN[SG=Female,NUM=sg,SEM=<\P.P(Beatrice)>] -> 'Beatrice'

# Verbs - singular
V[NUM=sg,SEM=<\X x.X(\y.love(x,y))>] -> 'ama'
V[NUM=sg,SEM=<\X x.X(\y.hate(x,y))>] -> 'odia'
V[NUM=sg,SEM=<\X x.X(\y.worship(x,y))>] -> 'adora'
V[NUM=sg,SEM=<\X x.X(\y.see(x,y))>] -> 'rivede'
# intransitive verbs
IV[NUM=sg,SEM=<\x.(sail(x))>] -> 'naviga'
# plural
# TODO: verificare il plurale
V[NUM=pl,SEM=<\X x.X(\y.dream(x,y))>] -> 'sognano'
V[NUM=pl,SEM=<\X x.X(\y.worship(x,y))>] -> 'adorano'
V[NUM=pl,SEM=<\X x.X(\y.visit(x,y))>] -> 'visitano'
DTV[NUM=pl,SEM=<\Y X x.X(\z.Y(\y.give(x,y,z)))>] -> 'lasciano'
DTV[NUM=pl,SEM=<\Y X x.X(\z.Y(\y.sell(x,y,z)))>] -> 'vendono'

# λ X.λ Y.λ R.(X(R) & Y(R))
CC[SEM=<\X \Y R.(X(R) & Y(R))>] -> 'e'

# TO -> a
P[+TO] -> 'a'
```

Le formule logiche generate:

    love(Paolo,Francesca)
    exists x.(man(x) & love(x,Francesca))
    exists z1.(man(z1) & hate(Francesca,z1))
    (dream(Paolo,Francesca) & dream(Paolo,Beatrice) & dream(Dante,Francesca) & dream(Dante,Beatrice))
    all x.(exists z2.(man(z2) & x(z2)) -> exists z3.(woman(z3) & worship(x,z3)))
    exists x.(man(x) & all z5.(exists z4.(woman(z4) & z5(z4)) -> worship(x,z5)))
    sail(Caronte)
    (exists z6.(hell(z6) & visit(Dante,z6)) & exists x.(man(x) & exists z6.(hell(z6) & visit(x,z6))))
    (exists x.(woman(x) & exists z7.(gift(z7) & give(x,z7,Virgilio))) & exists z7.(gift(z7) & give(Dante,z7,Virgilio)))
    exists z8.(star(z8) & see(Dante,z8))
    (exists z9.(man(z9) & sell(Dante,Beatrice,z9) & sell(Dante,Francesca,z9)) & exists z9.(man(z9) & sell(Virgilio,Beatrice,z9) & sell(Virgilio,Francesca,z9)))


### 4. Sentence Planner (Ruby)

Il sentence plan generato:

```
---
- clauses:
  - verb: love
    subj:
      noun: Paolo
      num: singular
    obj:
      noun: Francesca
      num: singular
- clauses:
  - verb: love
    subj:
      det: a
      noun: man
      num: singular
    obj:
      noun: Francesca
      num: singular
- clauses:
  - verb: hate
    subj:
      noun: Francesca
      num: singular
    obj:
      det: a
      noun: man
      num: singular
- clauses:
  - verb: dream
    subj:
      noun: Paolo
      num: singular
    obj:
      noun: Francesca
      num: singular
  - verb: dream
    subj:
      noun: Paolo
      num: singular
    obj:
      noun: Beatrice
      num: singular
  - verb: dream
    subj:
      noun: Dante
      num: singular
    obj:
      noun: Francesca
      num: singular
  - verb: dream
    subj:
      noun: Dante
      num: singular
    obj:
      noun: Beatrice
      num: singular
- clauses:
  - verb: worship
    subj:
      det: all
      noun: man
      num: plural
    obj:
      det: a
      noun: woman
      num: singular
- clauses:
  - verb: worship
    subj:
      det: a
      noun: man
      num: singular
    obj:
      det: all
      noun: woman
      num: plural
- clauses:
  - verb: sail
    subj:
      noun: Caronte
      num: singular
- clauses:
  - verb: visit
    subj:
      noun: Dante
      num: singular
    obj:
      det: a
      noun: hell
      num: singular
  - verb: visit
    subj:
      det: a
      noun: man
      num: singular
    obj:
      det: a
      noun: hell
      num: singular
- clauses:
  - verb: give
    subj:
      det: a
      noun: woman
      num: singular
    obj:
      det: a
      noun: gift
      num: singular
    pp:
      noun: Virgilio
      num: singular
      pre: to
  - verb: give
    subj:
      noun: Dante
      num: singular
    obj:
      det: a
      noun: gift
      num: singular
    pp:
      noun: Virgilio
      num: singular
      pre: to
- clauses:
  - verb: see
    subj:
      noun: Dante
      num: singular
    obj:
      det: a
      noun: star
      num: singular
- clauses:
  - verb: sell
    subj:
      noun: Dante
      num: singular
    obj:
      noun: Beatrice
      num: singular
    pp:
      det: a
      noun: man
      num: singular
      pre: to
  - verb: sell
    subj:
      noun: Dante
      num: singular
    obj:
      noun: Francesca
      num: singular
    pp:
      det: a
      noun: man
      num: singular
      pre: to
  - verb: sell
    subj:
      noun: Virgilio
      num: singular
    obj:
      noun: Beatrice
      num: singular
    pp:
      det: a
      noun: man
      num: singular
      pre: to
  - verb: sell
    subj:
      noun: Virgilio
      num: singular
    obj:
      noun: Francesca
      num: singular
    pp:
      det: a
      noun: man
      num: singular
      pre: to
```

### 5. Realiser (SimpleNLG/Java)

Le frasi tradotte in inglese:

    Paolo loves Francesca.
    A man loves Francesca.
    Francesca hates a man.
    Paolo dreams Francesca, Paolo dreams Beatrice, Dante dreams Francesca and Dante dreams Beatrice.
    All men worship a woman.
    A man worship all women.
    Caronte sails.
    Dante visits a hell and a man visits a hell.
    A woman give a gift to Virgilio and Dante gives a gift to Virgilio.
    Dante sees a star.
    Dante sells Beatrice to a man, Dante sells Francesca to a man, Virgilio sells Beatrice to a man and Virgilio sells Francesca to a man.
