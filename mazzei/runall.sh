echo "================"
echo "| cleaning all |"
echo "================"
sh ./clean.sh
echo 

echo "=================="
echo "| running cky.rb |"
echo "=================="
cd cky
ruby ./cky.rb
cd ..
echo

echo "======================"
echo "| running semtest.py |"
echo "======================"
cd semantic
python ./semtest.py
cd ..
echo 

echo "======================"
echo "| running planner.rb |"
echo "======================"
cd planner
ruby ./planner.rb
cd ..
echo

echo "============================"
echo "| compiling SRealiser.java |"
echo "============================"
cd realiser
sh ./compile.sh
cd ..
echo

echo "=========================="
echo "| running SRealiser.java |"
echo "=========================="
cd realiser
sh ./run.sh
cd ..