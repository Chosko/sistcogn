import simplenlg.framework.*;
import simplenlg.lexicon.*;
import simplenlg.realiser.english.*;
import simplenlg.phrasespec.*;
import simplenlg.features.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

public class SRealiser {
  public static void main(String[] args) throws IOException {
    // Initialize realiser
    Lexicon lexicon = Lexicon.getDefaultLexicon();
    NLGFactory nlgFactory = new NLGFactory(lexicon);
    Realiser realiser = new Realiser(lexicon);

    // Load sentence plan from file
    System.out.print("Loading sentence plan from file... ");
    InputStream input = new FileInputStream(new File("../data/sentenceplan.yml"));
    Yaml yaml = new Yaml();
    Object dataobj = yaml.load(input);
    System.out.println("ok");

    // The realised sentences
    String[] realised_sentences;
    int k = 0;

    // Foreach sentence
    System.out.print("Realising sentences... ");
    List<Map> sentences = (List<Map>)dataobj;
    realised_sentences = new String[sentences.size()];
    for (Map sentence : sentences) {

      // The sentence root phrase
      NLGElement start = null;

      // The clauses of the sentence
      SPhraseSpec[] sentence_clauses;
      int j = 0;

      // Foreach clause
      List<Map> clauses = (List<Map>)sentence.get("clauses");
      sentence_clauses = new SPhraseSpec[clauses.size()];
      for (Map clause : clauses) {
        
        // Create clause start
        SPhraseSpec s = nlgFactory.createClause();

        // Get/Set the verb
        String verb = (String)clause.get("verb");
        s.setVerb(verb);

        // Foreach phrase
        String[] phrases_names = {"subj", "obj", "pp"};
        Map[] phrases = {(Map)clause.get("subj"), (Map)clause.get("obj"), (Map)clause.get("pp")};
        for (int i=0; i<phrases.length; i++) {
          Map p = phrases[i];
          if(p != null){
            NPPhraseSpec np = nlgFactory.createNounPhrase();

            // Get/Set the det (determiner or specifier)
            String det = (String)p.get("det");
            if (det != null)
              np.setSpecifier(det);

            // Get/Set the noun
            String noun = (String)p.get("noun");
            if (noun != null)
              np.setNoun(noun);
            else
              throw new RuntimeException("noun not found for phrase: " + phrases_names[i]);

            // Get/Set if the phrase is singular or plural
            String num = (String)p.get("num");
            switch (num) {
              case "singular":
                np.setPlural(false);
              break;
              case "plural":
                np.setPlural(true);
              break;
              default:
                throw new RuntimeException(num + " is an invalid num for phrase: " + phrases_names[i]);
            }

            // Set the phrase
            switch (phrases_names[i]){
              case "subj":
                s.setSubject(np);
              break;
              case "obj":
                s.setObject(np);
              break;
              case "pp":
                String prep = (String)p.get("pre");
                if(prep != null){
                  PPPhraseSpec pp = nlgFactory.createPrepositionPhrase();
                  pp.addComplement(np);
                  pp.setPreposition(prep);
                  s.addComplement(pp);
                }
                else{
                  throw new RuntimeException("preposition not found for pp");
                }
              break;
            }
          }
        } // End foreach phrase
        sentence_clauses[j] = s;
        j++;
      } // End foreach clause
      
      // If multiple clauses exist set the root as coordinated phrase
      if (sentence_clauses.length > 1) {
        CoordinatedPhraseElement coord = nlgFactory.createCoordinatedPhrase();
        for (SPhraseSpec s : sentence_clauses) {
          coord.addCoordinate(s);
          start = coord;
        }
      }
      // If only one clause exist set the clause as the root
      else{
        start = sentence_clauses[0];
      }

      // Realise the phrases!
      realised_sentences[k] = realiser.realiseSentence(start);
      k++;
    } // End foreach sentence
    System.out.println("ok");

    // Store and display results
    System.out.println("Saving results to file... \n");
    PrintStream output = new PrintStream(new File("../data/sentences-02.txt"));
    for (String realised : realised_sentences) {
      System.out.println(realised);
      output.println(realised);
    }
  }
}
//end