# Semantica per G1

**Consegna**: Dotare di semantica la grammatica G1 (G2) e provarla in
NLTK

## Riferimenti

Il prof a lezione ha portato questo esempio:

    >>> from nltk import load_parser
    >>> parser = load_parser('grammars/book_grammars/simple-semit.fcfg', trace=0)
    >>> sentence = 'Paolo ama Francesca'
    >>> tokens = sentence.split()
    >>> trees = parser.nbest_parse(tokens)
    >>> for tree in trees:
    ...     print tree.node['SEM']
    ...
    love(paolo,francesca)

Nella riga 2, la semantica viene caricata dal file simple-semit.fcfg, che contiene:

    ## Natural Language Toolkit: A simple example for Italian
    ## Author: Alessandro Mazzei <mazzei@di.unito.it>
    ## To see more complex example -> simple-sem.fcfg
    % start S
    ############################
    # Grammar Rules
    #############################
    S[SEM = <?subj(?vp)>] -> NP[SEM=?subj] VP[SEM=?vp]
    NP[SEM=?np] -> PropN[SEM=?np]
    VP[SEM=<?v(?obj)>] -> V[SEM=?v] NP[SEM=?obj]
    #############################
    # Lexical Rules
    #############################
    PropN[SEM=<\P.P(paolo)>] -> 'Paolo'
    PropN[SEM=<\P.P(francesca)>] -> 'Francesca'
    V[SEM=<\X x.X(\y.love(x,y))>] -> 'ama'

Riferimenti del libro NLTK sulle grammatiche basate su features (fcfg)

* Capitolo 9: [Building Feature Based Grammars](http://www.nltk.org/book/ch09.html)
* Capitolo 10: [Analyzing the Meaning of Sentences](http://www.nltk.org/book/ch10.html)