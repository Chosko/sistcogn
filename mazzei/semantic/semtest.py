#!/usr/bin/python

# parsing the sentences using grammar + semantics
from nltk import load_parser
# loading semantics, watch out for working directory / trace 0 means no debug output
cp = load_parser('file:../data/g2.fcfg', trace=0)

# read sentences from file (stripping \n)
print 'Reading sentences from file...',
with open('../data/sentences-01.txt') as f:
  sentences = [line.rstrip() for line in f]
print "ok"

# First order Logic with semantics output
folsem = open('../data/folsem.txt','w')

print "Parsing semantics for sentences..."
for s in sentences:
  # split the sentence
  tokens = s.split()
  # parsing the best tree
  trees = cp.nbest_parse(tokens)
  # print the semantics for the current sentence
  print "Sentence: ", s
  for tree in trees:
    # ubild the semantic FoL sentence
    sem = str(tree.node['SEM']) + '\n'
    # print
    print "Semantics: ", sem
    # write to file output
    folsem.write(sem)
